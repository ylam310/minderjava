import java.lang.System;

class WatcherThread extends Thread {
  
  private int mCurrentState;
  private boolean mDone;
  private long mLastTimestamp;
  private int timeout;
  private WListener callback;
  //private int retries;
  
  public WatcherThread(int cs, WListener w) {
    mCurrentState = cs;
    mDone = false;
    mLastTimestamp = System.currentTimeMillis();
    timeout = 0;
    callback = w;
    //retries = 0;
  }
  
  
  public void run() {
    callback.log("watch thread starting");
    try {
      callback.log("Waiting for watcer to start");
      Thread.sleep(5000);
    } catch(InterruptedException e) {
        // Interrupted
        //mDone = true;
        callback.log("Watcher Thread Interrupted");
    }
    
    while(!mDone) {
      // We are connected and we should be checking how often we are getting data
      try {
        Thread.sleep(1000);
        if(mCurrentState == 1) {
          //callback.log("Checking on latest data");
          if(System.currentTimeMillis() - 1000 > mLastTimestamp) {
            callback.log("No data in 1 second...");
            timeout += 1;
            if(timeout == 5) {
              callback.log("no data in last 5 seconds...");
              callback.noData();
              mCurrentState = 0;
              timeout = 0;
            }
          } else {
            //callback.log("Received recent data");
          }
        } else if(mCurrentState == 0) {
          timeout += 1;
          callback.log("Waiting " + timeout + " seconds before reconnect");
          if(timeout == 5) {
            mDone = true;
            callback.reconnect();
          }
        }
      } catch(InterruptedException e) {
        // Interrupted
        //mDone = true;
        callback.log("Watcher Thread Interrupted");
      }
      
    }
    
    callback.log("watcher thread done");
  }
  
  
  public void gotData() {
    mLastTimestamp = System.currentTimeMillis();
  }
  
  public void stopWatcher() {
    mDone = true;
    this.interrupt();
  }
  
  public interface WListener {
    public void noData();
    public void log(String msg);
    public void reconnect();
  }
  
}
  