import java.io.FilenameFilter;
import java.io.FileWriter;
import java.io.BufferedWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;
import java.util.Iterator;
import java.net.URL;

class MinderHttp {
  
  private MinderInterface callback;
  private ErrorLogger elog;
  
  public MinderHttp(ErrorLogger logger, MinderCallback cb) {
    elog = logger;
    callback = cb;
  }
  
  // Public API calls 
  
  public void registerDevice(String oauth, String deviceId) {
    RegisterThread regger = new RegisterThread(oauth, deviceId);
    regger.start();
  }
  
  public void loginUser(String username, String password) {
    LoginThread login = new LoginThread(username, password);
    login.start();
  }
  
  public void updatePrograms(String oauth) {
    ProgramThread progger = new ProgramThread(oauth);
    progger.start();
  }
  
  public void uploadData(String oauth) {
    UploadThread uploader = new UploadThread(oauth);
    uploader.start();
  }
  
  public void sendActivityResult(String oauth, String res, String pid, String sid, String sdid, String mid, long stime, long ctime) {
    String stimes;
    String ctimes;
    SimpleDateFormat sdf = new SimpleDateFormat("YY-MM-dd HH:mm:ss");
    stimes = sdf.format(new Date(stime));
    ctimes = sdf.format(new Date(ctime));
    ResultThread resulter = new ResultThread(oauth, res, pid, sid, sdid, mid, stimes, ctimes);
    resulter.start();    
  }
  
  public void updateSensorSettings(String oauth, float sensitivity, float difficulty1, float difficulty2, int seatback, long tim) {
    UpdateSettingsThread supdater = new UpdateSettingsThread(oauth, sensitivity, difficulty1, difficulty2, seatback, tim);
    supdater.start();
  }
  
  // Worker threads
  
  private class RegisterThread extends Thread{ 

    private String sendString;
    
    public RegisterThread(String oauth, String deviceId) {
      sendString = "[{\"oauth_token\":\"" + oauth + "\",\"device_id\":\"" + deviceId + "\",\"overwrite\":\"true\"}]";
    }
    
    public void run() {
      String url = "http://ec2-52-88-29-130.us-west-2.compute.amazonaws.com/alphapro/api/setdeviceid";
      String result = execPost(sendString, url);
      // Parse result of the post
    }
    
  }
  
  private class LoginThread extends Thread {
    
    private String sendString;
    private String mUsername;
    
    public LoginThread(String username, String password) {
      sendString = "[{\"username\":\"" + username + "\",\"password\":\"" + password + "\"}]";
      mUsername = username;
    }
    
    public void run() {
      String url = "http://ec2-52-88-29-130.us-west-2.compute.amazonaws.com/alphapro/api/authenticate";
      String result = execPost(sendString, url);
      // Parse result of the login
      if(result != null) {
        JSONObject json = parseJSONObject(result);
        try {
          String tok = json.getString("oauth_token");
          String exp = json.getString("oauth_expiration_time");
          callback.loginResponse(true, mUsername, tok, exp);
        } catch(Exception ex) {
          callback.loginResponse(false, "","","");
        }
      } 
    }
  }
  
  private class ProgramThread extends Thread {
    
    private String sendString;
    private String mOauth_token;
    
    public ProgramThread(String oauth_token) {
      sendString = "[{\"oauth_token\":\"" + oauth_token + "\"}]";
      mOauth_token = oauth_token;
    }
    
    public void run() {
      String url = "http://ec2-52-88-29-130.us-west-2.compute.amazonaws.com/alphapro/api/programlist";
      String result = execPost(sendString,url);
      SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
      //String today = sdf.format(new Date(System.currentTimeMillis()+68400000));
      String today = sdf.format(new Date(System.currentTimeMillis()));
      JSONArray jsched = new JSONArray();
      
      if(result != null) {
        elog.log("Succeeded in getting list of programs, querying programs for activities");
        try {
          JSONArray jArray = parseJSONArray(result);
          for(int i = 0; i < jArray.size(); i++) {
            JSONObject json = jArray.getJSONObject(i);
            String pid = json.getString("program_id");
            elog.log("Found a program with id=" + pid + ", querying for activities");
            String url2 = "http://ec2-52-88-29-130.us-west-2.compute.amazonaws.com/alphapro/api/activities";
            String args2 = "[{\"oauth_token\":\"" + mOauth_token + "\",\"program_id\":\"" + pid + "\",\"for_date\":\"" + today + "\"}]";
            String actresults = minderhttp.execPost(args2, url2);
            String newactresults = actresults.replaceAll("exercise_schedule\":","exercise_schedule\":[");
            newactresults = newactresults.replaceAll("}}}]","}}]}]");
            //int ei = actresults.indexOf("exercise_schedule");
            //println("Found exercise_schedule at " + ei);
            elog.log("fixed program json string:\r\n"+newactresults);
            
            if(actresults != null) {
              try {
                JSONArray jActArray = parseJSONArray(actresults);
                for(int j = 0; j < jActArray.size(); j++) {
                  elog.log("Parsing first thingy");
                  JSONObject actjson = jActArray.getJSONObject(j);
                  String media_id = actjson.getString("media_id");
                  elog.log("media_id=" + media_id);
                  /*String time_period = actjson.getString("time_period");
                  String frequency = actjson.getString("frequency");
                  String from_time = actjson.getString("from_time");
                  String till_time = actjson.getString("till_time");*/
                  String media_type = actjson.getString("media_type");
                  elog.log("media_type=" + media_type);
                  String media_title = actjson.getString("media_title");
                  elog.log("media_title="+media_title);
                  String media_description = actjson.getString("media_description");
                  elog.log("media_description="+media_description);
                  //String acess = actjson.getString("acess");
                  String media_url = actjson.getString("media_url");
                  elog.log("media_url="+media_url);
                  //String csvfile_url = actjson.getString("csvfile_url");
                  String csvfile_url = actjson.getString("additional_file");
                  elog.log("additional_file="+csvfile_url);
                  //String difficulty_level = actjson.getString("difficulty_level");
                  //String sensitivity_level = actjson.getString("sensitivity_level");
                  String duration = actjson.getString("duration");
                  elog.log("duration=" + duration);
                  //JSONArray jexercise_schedule = actjson.getJSONArray("exercise_schedule");
                  JSONObject jexercise_schedule = actjson.getJSONObject("exercise_schedule");
                  elog.log("Found an activity schedued");
                  elog.log("Found activity with media_id = " + media_id + " title: " + media_title);
                  elog.log("Media URL: " + media_url);
                  elog.log("CSV URL: " + csvfile_url);
                  elog.log("Schedule: " + jexercise_schedule.toString());
                  String media_filename = downloadFile(media_url, "video");
                  String csv_filename = downloadFile(csvfile_url, "paths");
                  //for(int k = 0; k < jexercise_schedule.size(); k++) {
                  //Iterator<?> keys = jexercise_schedule.keys();
                  //while(keys.hasNext()) {
                  for(Object thekey : jexercise_schedule.keys()) {
                  //for(int k = 0; k < jexercise_schedule.names().length(); k++) {
                    JSONObject jactivity = new JSONObject();
                    elog.log("getting start time");
                    //String start_time = jexercise_schedule.getJSONObject(k).getString("start_time");
                    //String thekey = (String)keys.next();
                    jactivity.setString("program_id",pid);
                    JSONObject kactivity = jexercise_schedule.getJSONObject((String)thekey);
                    //String start_time = jexercise_schedule.getJSONObject((String)thekey).getString("start_time");
                    String start_time = kactivity.getString("start_time");
                    elog.log("start time = " + start_time);
                    String sid = kactivity.getString("schedule_id");
                    elog.log("schedule_id = " + sid);
                    String sdid = kactivity.getString("schedule_date_id");
                    elog.log("schedule_date_id = " + sdid);
                    String mid = kactivity.getInt("modified_id")+"";
                    elog.log("modified_id = " + mid);
                    elog.log("activity sid = " + sid + ", sdid=" + sdid + ", mid=" + mid + ", pid=" + pid);
                    jactivity.setString("name",media_title);
                    if(media_filename != null) {
                      jactivity.setString("video",media_filename);
                    } else {
                      jactivity.setString("video","video.mov");
                    }
                    if(csv_filename != null) {
                      jactivity.setString("path",csv_filename);
                    } else {
                      jactivity.setString("path","");
                    }
                    jactivity.setString("offset","0");
                    
                    jactivity.setString("pid",pid);
                    jactivity.setString("sid",sid);
                    jactivity.setString("sdid",sdid);
                    jactivity.setString("mid",mid);
                    
                    // Set the time
                    SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
                    Date theday = new Date();
                    String thedate = dateFormat.format(theday);
                    SimpleDateFormat sdf2 = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
                    sdf.setTimeZone(TimeZone.getTimeZone("PST"));
                    String fulldate = thedate + " " + start_time;
                    elog.log("fulldate = " + fulldate);
                    Date date = sdf2.parse(fulldate);
                    elog.log("Activity date: " + fulldate + " in milliseconds: " + date.getTime()); 
                    jactivity.setString("time",""+date.getTime());
                    
                    
                    /*String newtime = "" + (System.currentTimeMillis() + 20000 + k*300000);
                    
                    jactivity.setString("time",newtime);*/
                    jsched.append(jactivity);
                  }
                }
              } catch (Exception ex) {
                elog.log("Problem with this activity... move to the next");
              }
            } else {
              elog.log("No activities associated with this program");
            }
          }  
        } catch(Exception ex) {
          elog.log("problem with programs");
          elog.log( Arrays.toString(ex.getStackTrace()));
          File schedfile = dataFile("schedule.json");
          if(schedfile.exists()) {
            schedfile.delete();
          }
        }
      } else {
        elog.log("Failed to retrieve list of programs");
      }
      if(jsched.size() > 0) {
        String spath = dataPath("schedule.json");
        saveJSONArray(jsched, spath);
      }
    }
    
    private String downloadFile(String url, String type) {
      if(url.equals("")) {
        return  null;
      }
      String urlparts[] = split(url,'/');
      String filename = urlparts[urlparts.length-1];
      elog.log("download filename = " + urlparts[urlparts.length-1]);
      File mediafile = dataFile(type + "/" + urlparts[urlparts.length-1]);
      if(!mediafile.exists()) {
        elog.log("We don't have this file, lets download it");
        try {
        URL downloadurl = new URL(url);
        FileUtils.copyURLToFile(downloadurl,mediafile);
        elog.log("Downloaded the file");
        } catch (Exception e) {
          elog.log("problem with url");
          elog.log( Arrays.toString(e.getStackTrace()));
          return null;
        }
      } else {
        elog.log("we already have this file, no need to download it");
        return filename;
      }
      return filename;
    }
  }
  
  private class UploadThread extends Thread {
  
    PrintWriter output;
    PrintWriter zoutput;
    
    float totaly = 0;
    float totalr = 0;
    String mOauth_token;
    long starttime = 0;
    
    private String[] fieldNames = {"x","y","z","dx","dy","dz","degy","degz","yellow","red","u1","u2","t"};
    
    public UploadThread(String oauth_token) {
      mOauth_token = oauth_token;
    }
    
    public void run() {

      // Go through all of the files in the complete folder
      File folder = new File(dataPath("complete"));
      File[] listOfFiles = folder.listFiles(new FilenameFilter() {
        public boolean accept(File folder, String name) {
          return name.toLowerCase().endsWith(".csv");
        }
      });
      
      File f = dataFile("dailydata.csv");
      //File f = dataFile("data.csv");
      try {
        zoutput = new PrintWriter(new BufferedWriter(new FileWriter(f,true)));
      } catch (IOException e) {
        e.printStackTrace();
        elog.log( Arrays.toString(e.getStackTrace()));
      }
  
      for (int i = 0; i < listOfFiles.length; i++) {
        if (listOfFiles[i].isFile()) {
          String fullname = listOfFiles[i].getName();
          elog.log("Processing file " + fullname);
          
          
          String[] nameparts = split(fullname,".");
         
          float sum_y = 0.0;
          float sum_z = 0.0;
          float state = 0;
          int avg_index = 0;
          int jindex = 0;
          
          JSONArray jArray = new JSONArray();
          String[] lines;
          //lines = loadStrings(dataPath("complete/data.csv"));
          lines = loadStrings(dataPath("complete/" + fullname));
          output = createWriter(dataPath("complete/" + nameparts[0] + ".json"));
          
          elog.log("Converting to json");
          for (int k=0; k<lines.length; k++) {
            
            String[] part = split(lines[k],',');
            
            if(k == 0) {
              starttime = (long)Float.parseFloat(part[12]);
            }
            
            sum_y += Float.parseFloat(part[1]);
            sum_z += Float.parseFloat(part[2]);
            if(Float.parseFloat(part[8]) == 1) {
              state += 1;
              totaly += .1;
            } 
            if(Float.parseFloat(part[9]) == 1) {
              state += 1;
              totalr += .1;
              totaly -= .1;
            }
            avg_index += 1;
            
            if(avg_index == 30) {
              
              JSONObject j = new JSONObject();
              //j.setString("y",""+(int)(sum_y/30/4));
              //j.setString("z",""+(int)(sum_z/30/4));
              j.setString("y",""+(int)(sum_y/30.0));
              j.setString("z",""+(int)(sum_z/30.0));
              j.setString("state",""+round(state/30.0));
              j.setString("t",""+part[12]);
              //j.setString("device_id","testdevice123");
              j.setString("oauth_token",mOauth_token);
              j.setString("epoch_time",""+System.currentTimeMillis());
              jArray.setJSONObject(jindex,j);
              jindex += 1;
              avg_index = 0;
              sum_y = 0;
              sum_z = 0;
              state = 0;
            }
          }
          zoutput.write(starttime+","+Math.round((60-totaly-totalr)) +"," + Math.round(totaly) + "," + Math.round(totalr) + "\n");
          
          output.write(jArray.toString());
          output.close();
          
          File curfile = new File(dataPath("complete/" + fullname));
          curfile.delete();
          //File processedfile = dataFile("complete/uploaded/" + fullname);
          //curfile.renameTo(processedfile);
          
        } else if (listOfFiles[i].isDirectory()) {
          elog.log("Directory " + listOfFiles[i].getName());
        }
      }
      
      zoutput.flush();
      zoutput.close();
      
      // Go through all of the files in the complete folder
      File jsonFolder = new File(dataPath("complete"));
      File[] jsonFiles = jsonFolder.listFiles(new FilenameFilter() {
        public boolean accept(File jsonFolder, String jsonname) {
          return jsonname.toLowerCase().endsWith(".json");
        }
      });
      
     String url = "http://ec2-52-88-29-130.us-west-2.compute.amazonaws.com/alphapro/api/add";
      
     for (int i = 0; i < jsonFiles.length; i++) {
       if (jsonFiles[i].isFile()) {
          String fullname = jsonFiles[i].getName();
          elog.log("Uploading file " + fullname);
          
          String[] lines = lines = loadStrings(dataPath("complete/" + fullname));
          String sendString = "";
          for(int j = 0; j < lines.length; j++) {
            sendString += lines[j] + "\n";
          }
          //if(uploadJson(sendString)) {
          if(execPost(sendString,url) != null) {
            elog.log("Upload successful, deleting file");
            File delfile = dataFile("complete/" + fullname);
            delfile.delete();
            //File uploadedfile = dataFile("complete/uploaded/" + fullname);
            //delfile.renameTo(uploadedfile);
          }
        }
      }
    }
  }
  
  private class ResultThread extends Thread{ 

    private String sendString;
    
    public ResultThread(String oauth, String result, String pid, String sid, String sdid, String mid, String stime, String ctime) {
      sendString = "[{\"oauth_token\":\"" + oauth + "\",\"program_id\":\"" + pid + "\",\"schedule_id\":\"" + sid
                     + "\",\"schedule_date_id\":\"" + sdid
                     + "\",\"modified_id\":\"" + mid 
                     + "\",\"result\":\"" + result
                     + "\",\"started_time\":\"" + stime
                     + "\",\"completed_time\":\"" + ctime + "\"}]";
    }
    
    public void run() {
      elog.log("sendString = " + sendString);
      String url = "http://ec2-52-88-29-130.us-west-2.compute.amazonaws.com/alphapro/api/setexercisestatus";
      String result = execPost(sendString, url);
      // Parse result of the post
    }
    
  }
  
  private class UpdateSettingsThread extends Thread{ 

    private String sendString;
    
    public UpdateSettingsThread(String oauth, float sensitivity, float difficulty1, float difficulty2, int seatback, long tim) {
      sendString = "[{\"oauth_token\":\"" + oauth + "\",\"sensor_sensitivity\":\"" + sensitivity
                     + "\",\"sensor_threshold1\":\"" + difficulty1
                     + "\",\"sensor_threshold2\":\"" + difficulty2
                     + "\",\"sensor_leanback\":\"" + seatback
                     + "\",\"timestamp\":\"" + tim + "\"}]";
    }
    
    public void run() {
      elog.log("sendString = " + sendString);
      String url = "http://ec2-52-88-29-130.us-west-2.compute.amazonaws.com/alphapro/api/setsensorlevel";
      String result = execPost(sendString, url);
      // Parse result of the post
    }
    
  }
  
  public String execPost(String sendString, String url) {
    HttpClient httpClient = new DefaultHttpClient();
    HttpPost httppost = new HttpPost(url);
    String retval = null;
    try{
      StringEntity params = new StringEntity(sendString);
      httppost.addHeader("Content-Type", "application/json");
      httppost.addHeader("Accept","application/json");
      params.setContentEncoding(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
      httppost.setEntity(params);
      
      elog.log("Excuting POST to " + url);
      HttpResponse response = httpClient.execute(httppost);
      elog.log("Finished POST");
      ResponseHandler<String> handler = new BasicResponseHandler();
      String body = handler.handleResponse(response);
      int code = response.getStatusLine().getStatusCode();
      elog.log("httpresponse code: " + code);
      elog.log("httpresponse body: " + body);
      if(code == 200) {
        retval = body;
      }
    }catch (Exception ex) {
      //println("Exception: " + ex.getMessage());
      ex.printStackTrace();
      elog.log(Arrays.toString(ex.getStackTrace()));
        // handle exception here
    } finally {
        httpClient.getConnectionManager().shutdown();
    }
    return retval;
  }
}