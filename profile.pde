class Profile {
  float error_y;
  float error_z;
  float sensitivity;
  float percentage;
  float difficulty1;
  float difficulty2;
  int alert1sound;
  int alert2sound;
  int volume1;
  int volume2;
  int interval1;
  int interval2;
  int vpattern1;
  int vpattern2;
  int vstrength1;
  int vstrength2;
  int redyellow;
  int seatback;
  float seatback1;
  float seatback2;
  String name;
  int vib1;
  int vib2;
  
  Profile (String new_name) {
    error_y = 0;
    error_z = 0;
    sensitivity = 50;
    percentage = 50;
    name = new_name;
    difficulty1 = 30;
    difficulty2 = 60;
    alert1sound = 0;
    alert2sound = 1;
    volume1 = 20;
    volume2 = 20;
    interval1 = 3;
    interval2 = 3;
    vpattern1 = 0;
    vpattern2 = 1;
    vstrength1 = 20;
    vstrength2 = 20;
    redyellow = 1;
    seatback = 0;
    seatback1 = 30;
    seatback2 = 30;
    vib1 = 1;
    vib2 = 1;
  }
  
  Profile(String new_name, float new_y, float new_z, float new_sensitivity, float new_percentage) {
    error_y = new_y;
    error_z = new_z;
    sensitivity = new_sensitivity;
    percentage = new_percentage;
    name = new_name;
  }
  
  Profile(String new_name, float new_y, float new_z, float new_difficulty1, float new_difficulty2
          , int new_alert1sound, int new_alert2sound, int new_volume1, int new_volume2
          , int new_interval1, int new_interval2
          , int new_vpattern1, int new_vpattern2, int new_vstrength1, int new_vstrength2
          , int new_redyellow, int new_seatback, float new_sensitivity, float new_seatback1, float new_seatback2, int new_vib1, int new_vib2) {
    error_y = new_y;
    error_z = new_z;
    difficulty1 = new_difficulty1;
    difficulty2 = new_difficulty2;
    sensitivity = 70.0;
    name = new_name;
    alert1sound = new_alert1sound;
    alert2sound = new_alert2sound;
    volume1 = new_volume1;
    volume2 = new_volume2;
    interval1 = new_interval1;
    interval2 = new_interval2;
    vpattern1 = new_vpattern1;
    vpattern2 = new_vpattern2;
    vstrength1 = new_vstrength1;
    vstrength2 = new_vstrength2;
    redyellow = new_redyellow;
    seatback = new_seatback;
    sensitivity = new_sensitivity;
    seatback1 = new_seatback1;
    seatback2 = new_seatback2;
    vib1 = new_vib1;
    vib2 = new_vib2;
  }
    
  
  String toString() {
    return name+","+error_y+","+error_z+","+sensitivity+","+percentage; 
  }
  
  String toString2() {
    return name+","+error_y+","+error_z+","+difficulty1+","+difficulty2+","+alert1sound+","+alert2sound+","
              +volume1+","+volume2+","+interval1+","+interval2+","
              +vpattern1+","+vpattern2+","+vstrength1+","+vstrength2 + ","
              + redyellow + "," + seatback + "," + sensitivity + "," + seatback1 + "," + seatback2 + "," + vib1 + "," + vib2;
  }
}