import ddf.minim.spi.*;
import ddf.minim.signals.*;
import ddf.minim.*;
import ddf.minim.analysis.*;
import ddf.minim.ugens.*;
import ddf.minim.effects.*;
import java.awt.Point;

import processing.serial.*;
import processing.awt.PSurfaceAWT;
import java.awt.Frame;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import controlP5.*;
import ddf.minim.*;
import java.util.Arrays;
import processing.video.*;
import com.sun.awt.AWTUtilities;
import java.awt.Shape;
import java.awt.geom.Ellipse2D;
import java.awt.Rectangle;
//import com.hamoid.*;



PApplet app;
// GUI controller
ControlP5 cp5;
RadioButton profileRadio;
DropdownList dropdown;
//Group g2;
Button audioButton;
Button vibrationButton;
ControlFrame cf;
GuidanceFrame gf;
DataFrame df;
Button settingsButton;
Button guidanceButton;
Button dataButton;
Textlabel batteryText;
PSurfaceAWT.SmoothCanvas canvas;
Frame sframe;

Capture cam;
//VideoExport videoExport;

ErrorLogger elog;

// Alert threa
AlertThread yellowAlert;
WatcherThread watcher;
MinderHttp minderhttp;

Point pt;

PFont f;
// grey circle
PImage img1;
// green circle
PImage img2;

int windowX;
int windowY;
int windowXw;
int windowYw;

String name = "";
float sensitivity=50;
float oldsensitivity=sensitivity;
//float percentage=50;
float difficulty1;
float difficulty2;
int alert1sound;
int alert2sound;
int volume1;
int volume2;
int interval1;
int interval2;
int vpattern1;
int vpattern2;
int vstrength1;
int vstrength2;
int redyellow;
int seatback;
float seatback1;
float seatback2;
//boolean lastFocused=true;
int vib1;
int vib2;

int[] intervalTimes = {1, 5, 10, 30, 60, 120, 300};
String[] intervalNames = {"1 sec", "5 sec", "10 sec", "30 sec", "1 min", "2 min", "5 min"};
String[] soundNames = {"short","long"};

boolean g2IsOpen=false;
String username;
String password;
String oauth_token;
String oauth_expiry;
String vib_type;
//LoginCallback loginCallback = new LoginCallback();
MinderCallback minderCallback = new MinderCallback();

int curProfile = 0;
Profile[] profileList;
Profile[] profileList2;

float gcount = 0;
float ycount = 0;
float rcount = 0;

String port = "COM6";
int linecount = 0;
int bytecount;
long lastbytecount;

// tracks current state of program
State currentState = State.SETUP;

// library for audio
Minim minim;        
AudioPlayer soundPlayer;
AudioInput soundInput;

Movie myMovie;

// serial controller
Serial myPortAcc;
// output data file
PrintWriter output;
PrintWriter routput;
PrintWriter error;

// For recording...
boolean isRecording = false;
boolean isPlaying = false;
float comparex = 0;
float comparey = 0;
long playUntil = 0;
String[] playlines;
int playindex = 0;
float playscore1 = 0;
float playscore2 = 0;
String play_pid;
String play_sid;
String play_sdid;
String play_mid;
long play_stime;
long play_ctime;

boolean lastFocused = true;

// data buffers for timers
int[] flagcounter1 = new int[1800];
int[] flagcounter2 = new int[900];
int[] flagcounter3 = new int[450];
int[] flagcounter4 = new int[40];
float[] cali_y = new float[150];
float[] cali_z = new float[150];

// offsets based on profile calibration
float error_y;
float error_z;

// processing variables

// whether to write data
boolean start = false;   

boolean flag1 = false;
boolean flagAngle = false;
boolean flagCommand = false;
boolean flagBattery = false;
boolean flag_tilting = false;
boolean flag_tilting2 = false;
boolean flag_bus = false;
boolean flag1m = false;
boolean flag1m_former = false;
boolean flag30s = false;
boolean flag15s = false;
boolean flag2s = false;
boolean flag_note_timer=false;
boolean flag_calibrate=false;
// for tracking ALT+C to start calibration
boolean isAltPressed = false;
boolean isCPressed = false;
boolean alarmActive = true;
boolean valarmActive = true;
boolean showAlertSettings = true;

boolean formergroup_open= true;
boolean formercontroller_open= true;

int [][] sumdata = new int[24][3];
String lastrecname;

// track calibration success/fail
boolean cali_done =true;

// notification timer
int note_timer=0;

// tracks start of calibration time
int calibrate_timer=0;

// calibration counter
int cali_cnt=0;
int cnt=0;
int cnt1=0;
int cnt2=0;
int cnt3=0;
int cnt4=0;
int former=0;
int x1=0;
int x2=0;
int y1=0;
int y2=0;
int z1=0;
int z2=0;
int dropFont=10;
float x=0;
float y=0;
float z=0;
float x_former=0;
float y_former=0;
float z_former=0;
float r=0;
float dx=0;
float dy=0;
float dz=0;
float y0=0;
float z0=0;
float dyy_former=0;
float dzz_former=0;
float display_x=0;
float display_y=0;
ArrayList<Float> pdisplay_x;
ArrayList<Float> pdisplay_y;

int videoFrameCount = 0;
int countdown = 0;
String exerciseName = "";

float deviation=0;

String[] lines;
ArrayList<String> whiports;

String profileName = "profiles.txt";
String profile2Name = "profiles2.txt";
String configName = "configuration.txt";
String dataName = "data.csv";
String image1Name = "image1.png";
String image2Name = "image2.png";
String[] qqName = {"beep-07.mp3", "qq.MP3"};

String deviceid = "";
int retries = 0;

ScheduleThread scheduler;
ExerciseThread exerciser;

WatcherThread.WListener wListener = new WatcherThread.WListener() {
  public void noData() {
    elog.log("Minder received message that there's no data! Closing serial port");
    if(yellowAlert != null) {
      yellowAlert.stopAlert();
    }

    if(myPortAcc != null) {
      myPortAcc.stop();
    }
    currentState = State.ERROR;
  }
  
  public void log(String msg) {
    elog.log(msg);
  }
  
  public void reconnect() {
    retries += 1;
    if(retries < 5) {
      elog.log("I should probably try to reconnect now...");
      startSerial();
    } else {
      elog.log("retried 5 times, stopping");
    }
    
  }
};


void setup() {
  size(350,300);
  surface.setAlwaysOnTop(true);
  surface.setResizable(true);
  //surface.setLocation(800,400);
  //frame.setAlwaysOnTop(true);
  canvas = (PSurfaceAWT.SmoothCanvas) surface.getNative();
  sframe = canvas.getFrame();
  
  /*sframe.removeNotify();
  sframe.setUndecorated(true);
  AWTUtilities.setWindowOpaque(sframe, false);
  AWTUtilities.setWindowOpacity(sframe, 0.5f);
  sframe.addNotify();*/
  
  //sframe.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
  /*
  sframe.removeNotify();
  sframe.setUndecorated(true);
  //AWTUtilities.setWindowOpaque(sframe, false);
  //AWTUtilities.setWindowOpacity(sframe, 0.5f);
  sframe.addNotify();
  
  Shape shape = null;
  shape = new Ellipse2D.Float(0, 0, 300, 300);
  AWTUtilities.setWindowShape(sframe, shape);
  */
  
  elog = new ErrorLogger(System.currentTimeMillis() + "_error.log");
  
  app = this;
  minderhttp = new MinderHttp(elog, minderCallback);
  deviceid = System.getProperty("user.name");
  elog.log("deviceid = " + deviceid);
  // load saved configuration file if exists
  loadConfig(configName);
    
  // config audio
  minim = new Minim(this);
  //soundPlayer = minim.loadFile("qq.MP3",2048);
  //soundPlayer = minim.loadFile(dataPath(qqName[alert1sound]),2048);
     
  // load saved profile list if exists
  //profileList = new Profile[5];
  profileList2 = new Profile[5];
  //loadProfiles(profileName);
  loadProfiles2(profile2Name);

  // set starting state based on last profile used
  //sensitivity = profileList[curProfile].sensitivity;
  //percentage = profileList[curProfile].percentage;    
  error_y = profileList2[curProfile].error_y;
  error_z = profileList2[curProfile].error_z;
  
  
  // set new settings
  name = profileList2[curProfile].name;
  difficulty1 = profileList2[curProfile].difficulty1;
  difficulty2 = profileList2[curProfile].difficulty2;
  alert1sound = profileList2[curProfile].alert1sound;
  alert2sound = profileList2[curProfile].alert2sound;
  volume1 = profileList2[curProfile].volume1;
  volume2 = profileList2[curProfile].volume2;
  interval1 = profileList2[curProfile].interval1;
  interval2 = profileList2[curProfile].interval2;
  vpattern1 = profileList2[curProfile].vpattern1;
  vpattern2 = profileList2[curProfile].vpattern2;
  vstrength1 = profileList2[curProfile].vstrength1;
  vstrength2 = profileList2[curProfile].vstrength2;
  redyellow = profileList2[curProfile].redyellow;
  seatback = profileList2[curProfile].seatback;
  sensitivity = profileList2[curProfile].sensitivity;
  seatback1 = profileList2[curProfile].seatback1;
  seatback2 = profileList2[curProfile].seatback2;
  vib1 = profileList2[curProfile].vib1;
  vib2 = profileList2[curProfile].vib2;
  
  //yellowAlert = new AlertThread(profileList2[curProfile].interval1*1000, 1000*profileList2[curProfile].interval2, qqName[alert1sound]);
  //yellowAlert.start();
  // enable sound
  //soundInput = minim.getLineIn();
  //output = createWriter("data.csv");
  output = createWriter(dataPath(dataName));
    
  cp5 = new ControlP5(this);
  
  settingsButton = cp5.addButton("Settings").setBroadcast(false)
    .setValue(0)
    .setPosition(290,85)//.setPosition(5,5)
    .setSize(55,19).setBroadcast(true);
    
  batteryText = cp5.addTextlabel("battery")
                    .setText("0")
                    .setPosition(5,5);
  
  cp5.addButton("Close").setBroadcast(false)
    .setValue(50)
    .setPosition(290,5)
    .setSize(55,19).setBroadcast(true);;
    
  cp5.addButton("Hide").setBroadcast(false)
    .setValue(75)
    .setPosition(290,30)
    .setSize(55,19).setBroadcast(true);;
    
  guidanceButton = cp5.addButton("Guidance").setBroadcast(false)
    .setValue(100)
    .setPosition(290,110)//.setPosition(250,5)
    .setSize(55,19).setBroadcast(true);
    
  audioButton = cp5.addButton("Audio").setBroadcast(false)
    .setValue(200)
    .setPosition(290,160)//.setPosition(240,270)
    .setSize(55,19)
    .setCaptionLabel("Sound").setBroadcast(true);
    
  vibrationButton = cp5.addButton("Vibration").setBroadcast(false)
    .setValue(400)
    .setPosition(290,185)//.setPosition(180,270)
    .setSize(55,19)
    .setCaptionLabel("Vibration").setBroadcast(true);
    
  guidanceButton = cp5.addButton("Data").setBroadcast(false)
    .setValue(300)
    .setPosition(290,135)//.setPosition(5,270)
    .setSize(55,19).setBroadcast(true);
    
  
  img1 = loadImage(dataPath(image1Name));
  img2 = loadImage(dataPath(image2Name));
  
  // set frame rate
  frameRate(10);
  
  // config font
  f = createFont("Arial", 16, true);
  
  currentState = State.DATACOLLECT;

  startSerial();
  
  if(currentState == State.DATACOLLECT) {
    currentState = State.STARTCALIBRATION;
  }
  
  elog.log("The object type is " + surface.getNative());
  /*canvas = (PSurfaceAWT.SmoothCanvas) surface.getNative();
  sframe = canvas.getFrame();
  
  frame.removeNotify();
  frame.setUndecorated(true);
  AWTUtilities.setWindowOpaque(frame, false);
  AWTUtilities.setWindowOpacity(frame, 0.9f);
  frame.addNotify();
  */
  /*
 
  sframe.addWindowFocusListener(new WindowAdapter() {
   public void windowGainedFocus(WindowEvent e) {
     println("gained focus");
     sframe.dispose();
     sframe.setUndecorated(false);
     //sframe.pack();
     sframe.setVisible(true);
   }
   
   public void windowLostFocus(WindowEvent e) {
     println("lost focus");
     sframe.dispose();
     sframe.setUndecorated(true);
     //sframe.pack();
     sframe.setVisible(true);
   }
    
  });*/
  
  elog.log("location is " + sframe.getLocationOnScreen());
  
  /*sframe.dispose();
  sframe.setUndecorated(true);
  sframe.setVisible(true);*/
  
  if(oauth_token != null && !oauth_token.equals("")) {
      minderhttp.updatePrograms(oauth_token);
  }
  
  scheduler = new ScheduleThread();
  scheduler.start();
  
  if(oauth_token == null || oauth_token.equals("")) {
    if(cf == null) {
        pt = sframe.getLocationOnScreen();
        cf = new ControlFrame(this,600,400,"App Settings");
      }
  }
  //videoExport = new VideoExport(this, "basic.mp4");
}


void draw() {
  background(0);
  //fill(0,0,0,0);
  //int c = color(175, 100, 220, 100);
  //fill(c);
  fill(255);
  
 

  switch(currentState) {
    case ERROR:
      textSize(12);
      text("Serial Port " + port + " could not be read.\n Please check your configuration file\n or restart the sensor",10,150);
      //noLoop();
      break;
    case STARTCALIBRATION:
      //println("starting calibrate");
      startCalibration();
      break;
    case CALIBRATION:
      //println("calibrating");
      performCalibration();
      displayCalibrationTimer();
      break;
    case FINISHCALIBRATION:
      //println("finish calibration");
      finishCalibration();
      displayCalibrationTimer();
      break;
    case CALIBRATIONRESULT:
      //println("showing calibration result");
      calibrationResult();
      displayCalibrationTimer();
      break;
    case DATACOLLECT:
      //println("in data collection mode");
      dataCollection();
      break;
    default:
      //println("in default... we shouldn't be here");
      dataCollection();
      break;
  }
  /*println("focused="+focused);
  if(focused != lastFocused) {
      println("Changing the window decoration visibility to " + focused);
      //canvas = (PSurfaceAWT.SmoothCanvas) surface.getNative();
      //sframe = canvas.getFrame();
      sframe.dispose();
      sframe.removeNotify();
      sframe.setUndecorated(!focused);
      sframe.addNotify();
      sframe.setVisible(true);
      lastFocused = focused;
  }*/
  
  //videoExport.saveFrame();
}

/*
void mousePressed(MouseEvent e) {
  windowX = e.getX();
  windowY = e.getY();
  println("new x,y = " + windowX + "," + windowY);
  windowXw = sframe.getLocation().x;
  windowYw = sframe.getLocation().y;
}

void mouseDragged(MouseEvent e) {
  int newx = e.getX();
  int newy = e.getY();
  int deltaX = newx - windowX;
  int deltaY = newy - windowY;
  println("dragged x,y = " + newx + "," + newy);
  
  sframe.setLocation(windowXw+deltaX, windowYw+deltaY);

  //windowX = newx;
  //windowY = newy;
} */

/*
void focusLost() {
//void windowLostFocus() {
  elog.log("Lost focus");
  Shape shape = null;
  shape = new Ellipse2D.Float(0, 0, 300, 300);
  AWTUtilities.setWindowShape(sframe, shape);
  vibrationButton.hide();
}

//void windowGainedFocus() {
void focusGained() {
  elog.log("Gained focus");
  Shape shape = null;
  shape = new Rectangle(0, 0, 350, 300);
  AWTUtilities.setWindowShape(sframe, shape);
  vibrationButton.show();
}*/

class GuidanceFrame extends PApplet {

  int w, h;
  PApplet parent;
  public ControlP5 cp5c;
  ListBox lb;
  //Capture cam;
  //Movie myMovie;
  boolean mOpened;

  public GuidanceFrame(PApplet _parent, int _w, int _h, String _name, boolean opened) {
    super();   
    parent = _parent;
    w=_w;
    h=_h;
    mOpened = opened;
    PApplet.runSketch(new String[]{this.getClass().getName()}, this);
  }

  public void setup() {
    this.surface.setSize(w, h);
    //this.surface.setSize(640,480);
    int ypos;
    ypos = pt.y - 340;
    if(ypos < 0) {
      ypos = pt.y + 340;
    }
    this.surface.setLocation(pt.x,ypos);
    
    cp5c = new ControlP5(this);
    cp5c.addButton("recordtoggle").setBroadcast(false)
    .setValue(0).setCaptionLabel("Record")
    .setPosition(25,10)
    .setSize(100,30).plugTo(parent).setBroadcast(true);
    
    cp5c.addButton("playtoggle").setBroadcast(false)
    .setValue(1).setCaptionLabel("Play")
    .setPosition(175,10)
    .setSize(100,30).plugTo(parent).setBroadcast(true);
    
    lb = cp5c.addListBox("myActivities")
        .setPosition(10,50).setSize(280,240)
        .setItemHeight(15).setBarHeight(15).setOpen(mOpened);
        
    //ArrayList<String> times = new ArrayList<String>();
    
    File schedfile = app.dataFile("schedule.json");
    if(schedfile.exists()) {
      JSONArray jsched = loadJSONArray(app.dataPath("schedule.json"));
      for(int i = 0; i < jsched.size(); i++) {
        JSONObject obj = jsched.getJSONObject(i);
        String ename = obj.getString("name");
        String etime = obj.getString("time");
        
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
        String thetime = sdf.format(new Date(Long.parseLong(etime)));
        lb.addItem(ename + " @ " + thetime, i);
      }
    }
    
    
    //cam = new Capture(this, 640,480);
    //String[] cameras = Capture.list();
    /*
    if (cameras.length == 0) {
      println("There are no cameras available for capture.");
      exit();
    } else {
      println("Available cameras:");
      for (int i = 0; i < cameras.length; i++) {
        println(cameras[i]);
      }
      cam = new Capture(this, cameras[6]);
      cam.start();
    }     */
    //cam = new Capture(this, 320, 180, 30);
    //cam.start();
    //videoExport = new VideoExport(this, "basic.mp4");
  }
  
  public void draw() {
    background(0);
    
    
    //scale(-1,1);
    /*if(isRecording) {
      if (cam.available() == true) {
        cam.read();
      }
      image(cam, 0, 55, 300, 225);
    } else*/ 
    /*if(isPlaying) {
      image(myMovie, 0, 55, 300, 225);
      
    }*/
    if(myMovie != null && isPlaying) {
      if (System.currentTimeMillis() < playUntil) {
        image(myMovie, 0, 55, 300, 225);
      } else {
        isPlaying = false;
        elog.log("Movie playback complete, no path score = " + playscore1);
        isPlaying = false;
        playindex = 0;
        ((Button)gf.cp5c.getController("playtoggle")).setCaptionLabel("Play");
        pdisplay_x.clear();
        pdisplay_y.clear();
        int playresult = 0;
        float movement = playscore1 / ((float)myMovie.duration());
        elog.log("Movement = " + movement);
        if(movement > 10) {
          playresult = 1;
        }
        play_ctime = System.currentTimeMillis();
        minderhttp.sendActivityResult(oauth_token, ""+playresult, play_pid, play_sid, play_sdid, play_mid, play_stime, play_ctime);
        
        myMovie = null;
      }
    }
    if(!isRecording && playscore1 != 0) {
      //DecimalFormat df = new DecimalFormat("#.##");
      //String score = 
      textSize(15);
      text("Score: " + (int)Math.round(playscore1) , 10, 285);
    }
    
    if(countdown > 0) {
      textSize(15);
      text(exerciseName, 10, 110);
      text("Starting in " + countdown, 10, 130);
    }
      
    //if(isRecording) {
    //saveFrame();
    //if(isRecording) {
      //save(dataPath("video/vid_" + videoFrameCount + ".jpg"));
    //}
    //videoExport.saveFrame();
  }
  
  
  void exit() {
    this.dispose();
    gf = null;
  }

}

class DataFrame extends PApplet {

  int w, h;
  PApplet parent;
  public ControlP5 cp5d;
  int[] angles = {120, 120, 120};
  float totalTime;

  public DataFrame(PApplet _parent, int _w, int _h, String _name) {
    super();
    parent = _parent;
    w=_w;
    h=_h;
  
    
    
    PApplet.runSketch(new String[]{this.getClass().getName()}, this);
  }

  public void setup() {
    this.surface.setSize(w, h);
    int xpos;
    xpos = pt.x - 600;
    if(xpos < 0) {
      xpos = pt.x + 320;
    }
    this.surface.setLocation(xpos,pt.y);
    frameRate(1);
    
    totalTime = gcount + ycount + rcount;
    cp5d = new ControlP5(this);
    cp5d.addTextlabel("gpercent")
                    .setText("Green Position: " + (int)gcount + "s (" + (int)(100 * gcount / totalTime) + "%)")
                    .setPosition(17,20).setColor(color(0,255,0))
                    ;
    
    cp5d.addTextlabel("ypercent")
                    .setText("Yellow Position: " + (int)ycount + "s (" + (int)(100 * ycount / totalTime) + "%)")
                    .setPosition(17,40).setColor(color(255,255,0))
                    ;
                    
    cp5d.addTextlabel("rpercent")
                    .setText("Red Position: " + (int)rcount + "s (" + (int)(100 * rcount / totalTime) + "%)")
                    .setPosition(17,60).setColor(color(255,0,0))
                    ;
    angles[0] = (int)Math.round(360 * rcount / totalTime);
    angles[1] = (int)Math.round(360 * ycount / totalTime);
    angles[2] = (int)Math.round(360 * gcount / totalTime);
    
    
  }
  
  public void draw() {
    background(0);
    
    for(int i = 0; i < 24; i++) {
      if(sumdata[i][0] >0 || sumdata[i][1] > 0 || sumdata[i][2] > 0) {
        long g = Math.round(90.0*(double)sumdata[i][0]/60.0);
        long y = Math.round(90.0*(double)sumdata[i][1]/60.0);
        long r = Math.round(90.0*(double)sumdata[i][2]/60.0);
        fill(255,0,0);
        rect(280 + i*13, 10, 12, r);
        fill(255,255,0);
        rect(280 + i*13, 10+r, 12, y);
        fill(0,255,0);
        rect(280 + i*13, 10+r+y, 12, g);
      } else {
        // do something?
      }
    }
    totalTime = gcount + ycount + rcount;
    angles[0] = (int)Math.round(360 * rcount / totalTime);
    angles[1] = (int)Math.round(360 * ycount / totalTime);
    angles[2] = (int)Math.round(360 * gcount / totalTime);
    
    pieChart(90, angles);
  }
  
  void pieChart(float diameter, int[] data) {
    float lastAngle = 0;
    for (int i = 0; i < data.length; i++) {
      fill(255,0,0);
      if(i == 1) fill(255,255,0);
      if(i == 2) fill(0,255,0);
      arc(200, 55, diameter, diameter, lastAngle, lastAngle+radians(angles[i]));
      lastAngle += radians(angles[i]);
    }
  }
  
  
  void exit() {
    this.dispose();
    df = null;
  }

}
  
class ControlFrame extends PApplet {

  int w, h;
  PApplet parent;
  public ControlP5 cp5b;
  public CheckBox cb;
  public CheckBox sbcb;
  public CheckBox vb1;
  public CheckBox vb2;

  public ControlFrame(PApplet _parent, int _w, int _h, String _name) {
    super();   
    parent = _parent;
    w=_w;
    h=_h;
    PApplet.runSketch(new String[]{this.getClass().getName()}, this);
  }

  public void setup() {
    this.surface.setSize(w, h);
    int xpos;
    xpos = pt.x - 400;
    if(xpos < 0) {
      xpos = pt.x + 320;
    }
    this.surface.setLocation(xpos,pt.y);
    cp5b = new ControlP5(this);
    cp5b.addTab("Basic");
    cp5b.addTab("Advanced");
    
    cp5b.getTab("default")
      .setLabel("Login");
    
    profileRadio = cp5b.addRadioButton("radio")
     .setPosition(10,50)
     .setSize(20,12).moveTo("Basic")
     //.addItem("P-1",0)
     //.addItem("P-2",1)
     //.addItem("P-3",2)
     //.addItem("P-4",3)
     //.addItem("P-5",4)
     ;
    for (int i = 0; i < 5; i++) {
      profileRadio.addItem(profileList2[i].name,i);
    }
    profileRadio.activate(curProfile).plugTo(parent,"radio");
    
    
    cp5b.addTextlabel("yz")
                    .setText("Yellow Zone Difficulty")
                    .setPosition(10,190).moveTo("Basic")
                    ; 
    cp5b.addSlider("difficulty1").setCaptionLabel("%").setValue(difficulty1)
                                .setRange(0, 100).setPosition(10,210).setSize(120,10)
                                .plugTo(parent).moveTo("Basic")
                                ;                
    cp5b.addTextlabel("rz")
                    .setText("Red Zone Difficulty")
                    .setPosition(10,245).moveTo("Basic")
                    ; 
    cp5b.addSlider("difficulty2").setCaptionLabel("%").setValue(difficulty2)
                                .setRange(0, 100).setPosition(10,265).setSize(120,10)
                                .plugTo(parent).moveTo("Basic")
                                ;    
    
    cp5b.addTextlabel("sb1")
                    .setText("Seatback Front Difficulty")
                    .setPosition(180,210).moveTo("Basic")
                    ; 
    cp5b.addSlider("seatback1").setCaptionLabel("%").setValue(seatback1)
                                .setRange(0, 100).setPosition(180,230).setSize(120,10)
                                .plugTo(parent).moveTo("Basic")
                                ;      
                                
    cp5b.addTextlabel("sb2")
                    .setText("Seatback Back Difficulty").moveTo("Basic")
                    .setPosition(390,210).hide();
                    ; 
    cp5b.addSlider("seatback2").setCaptionLabel("%").setValue(seatback1).moveTo("Basic")
                                .setRange(0, 100).setPosition(390,230).setSize(120,10)
                                .plugTo(parent).hide()
                                ;  
                                
    cp5b.addTextlabel("verlabel").setText("v7.2").setPosition(370,10)
                                .plugTo(parent).moveTo("Basic");
                                
    cp5b.addSlider("sensitivity").setCaptionLabel("Sensitivity").setValue(sensitivity)
                                 .setRange(0,100).setPosition(10,30).setSize(120,10)
                                 .plugTo(parent).moveTo("Advanced");
                                
    cb = cp5b.addCheckBox("redtoyellow").setPosition(150,265)
                                    .addItem("Red Scales with Yellow",0)
                                    .plugTo(parent).moveTo("Basic");
    if(redyellow == 1) {
      cb.activate(0);
    } else {
      cb.deactivate(0);
    }
    
    sbcb = cp5b.addCheckBox("seatback").setPosition(300,265)
                                       .addItem("Seat Back",0)
                                       .plugTo(parent).moveTo("Basic");
                                       
    if(seatback == 1) {
      sbcb.activate(0);
    } else {
      sbcb.deactivate(0);
    }
    
    cp5b.addTextlabel("soundalerts")
                    .setText("Sound Alerts")
                    .setPosition(180,15)
                    .setColor(color(255,0,0)).moveTo("Basic")
                    ;
    cp5b.addTextlabel("yellow1")
                    .setText("Yellow Zone")
                    .setPosition(180,35).moveTo("Basic")
                    ;
    cp5b.addSlider("volume1").setCaptionLabel("Volume").setRange(0, 100).setValue(volume1)
                              .setPosition(180,110).setSize(150,10)
                              .plugTo(parent).moveTo("Basic")
                              ;
    cp5b.addDropdownList("alert1sound").setCaptionLabel(soundNames[alert1sound]).setPosition(180,90)
                                      .addItem("Short",0).addItem("Long",1).setValue(alert1sound)
                                       .plugTo(parent).close().moveTo("Basic");                 
    
    vb1 = cp5b.addCheckBox("vib1enable").setPosition(285,90)
                                    .addItem("Vibrate",0)
                                    .plugTo(parent).moveTo("Basic");
   
   if(vib1 == 1) {
     vb1.activate(0);
   } else {
     vb1.deactivate(0);
   }
    
    cp5b.addDropdownList("interval1").setCaptionLabel(intervalNames[interval1]).setPosition(180,70).setValue(interval1)
                                      .addItem("1 sec",0).addItem("5 sec",1).addItem("10 sec",2).addItem("30 sec",3)
                                      .addItem("1 min",4).addItem("2 min",5).addItem("5 min",6)
                                      .plugTo(parent).close().moveTo("Basic");
                    
    cp5b.addTextlabel("i1text")
                    .setText("1st Interval")
                    .setPosition(180,55).moveTo("Basic")
                    ;                                  
    cp5b.addDropdownList("interval2").setCaptionLabel(intervalNames[interval2]).setValue(interval2).setPosition(285,70)
                                     .addItem("1 sec",0).addItem("5 sec",1).addItem("10 sec",2).addItem("30 sec",3)
                                     .addItem("1 min",4).addItem("2 min",5).addItem("5 min",6)
                                     .plugTo(parent).close().moveTo("Basic");
    cp5b.addTextlabel("i2text")
                    .setText("2nd Interval")
                    .setPosition(285,55).moveTo("Basic")
                    ;   
    
    
    cp5b.addTextlabel("alert2")
                    .setText("Red Zone")
                    .setPosition(180,140).moveTo("Basic")
                    ;
    cp5b.addSlider("volume2").setCaptionLabel("Volume").setRange(0, 100).setValue(volume2)
                              .setPosition(180,180).setSize(150,10).plugTo(parent).moveTo("Basic");
     
      
    cp5b.addDropdownList("alert2sound").setCaptionLabel(soundNames[alert2sound]).setPosition(180,160)
                                        .addItem("Short",0).addItem("Long",1).setValue(alert2sound)
                                        .plugTo(parent).close().moveTo("Basic"); 
    
   
    vb2 = cp5b.addCheckBox("vib2enable").setPosition(285,160)
                                    .addItem("Vibrate ",0)
                                    .plugTo(parent).moveTo("Basic");
                                    
     if(vib2 == 1) {
       vb2.activate(0);
     } else {
       vb2.deactivate(0);
     }
     
     // Vibration STuff
     cp5b.addTextlabel("vioalerts")
                    .setText("Vibration Alerts")
                    .setPosition(390,15)
                    .setColor(color(255,0,0)).hide().moveTo("Basic");
                    ;
    cp5b.addTextlabel("vyellow1")
                    .setText("Yellow Zone")
                    .setPosition(390,35).hide().moveTo("Basic");
                    ;
    
    cp5b.addSlider("vstrength1").setCaptionLabel("Strength").setRange(0, 100).setValue(vstrength1)
                                  .setPosition(390,110).setSize(150,10).plugTo(parent).hide().moveTo("Basic");
                                  
    cp5b.addDropdownList("vpattern1").setCaptionLabel(soundNames[vpattern1]).setPosition(390,90)
                                        .addItem("Off",0).addItem("On",1)
                                        .setValue(vpattern1).plugTo(parent).close().hide().moveTo("Basic"); 
     
    cp5b.addTextlabel("valert2")
                    .setText("Red Zone")
                    .setPosition(390,140).hide().moveTo("Basic")
                    ;
    cp5b.addSlider("vstrength2").setCaptionLabel("Strenth").setRange(0, 100).setValue(vstrength2)
                                  .setPosition(390,180).setSize(150,10).plugTo(parent).hide().moveTo("Basic");
                                  
    cp5b.addDropdownList("vpattern2").setCaptionLabel(soundNames[vpattern2]).setPosition(390,160)
                                        .addItem("Off",0).addItem("On",1)
                                        .setValue(vpattern2).plugTo(parent).close().hide().moveTo("Basic"); 
                                        
    cp5b.addTextfield("name").setPosition(10,135).setSize(50,19).setValue(name).setCaptionLabel("Activity Name")
                              .plugTo(parent).moveTo("Basic");
                              
    cp5b.addButton("updatename").setPosition(65,135).setSize(70,19).setCaptionLabel("Update Name")
                                .plugTo(parent).moveTo("Basic");
                                
    cp5b.addTextfield("username").setPosition(150,60).setSize(100,19).setValue(username).setCaptionLabel("Username")
                                .plugTo(parent).moveTo("default");
                                
    cp5b.addTextfield("password").setPosition(150,110).setSize(100,19).setValue("").setCaptionLabel("Password")
                                .setPasswordMode(true).plugTo(parent).moveTo("default");
    
    cp5b.addButton("login").setPosition(150,170).setSize(100,19).setCaptionLabel("Login")
                                .plugTo(parent).moveTo("default");
    String loginmsg = "Not logged in";
    if(!oauth_token.equals("")) {
      loginmsg = "Logged in";
      cp5b.getTab("Basic").setActive(true);
      cp5b.getTab("default").setActive(false);
      cp5b.getTab("Advanced").setActive(false);
    } 
    cp5b.addTextlabel("loginmessage").setText(loginmsg).setPosition(150,150)
                                .plugTo(parent).moveTo("default");
                                
    cp5b.addButton("logout").setPosition(150,190).setSize(100,19).setCaptionLabel("logout")
                                .plugTo(parent).moveTo("default");
     
    cp5b.addButton("programs").setPosition(150,220).setSize(100,19).setCaptionLabel("Get Program")
                                .plugTo(parent).moveTo("default");
                                
    cp5b.addButton("register").setPosition(150,250).setSize(100,19).setCaptionLabel("Register Device")
                                .plugTo(parent).moveTo("default");
                                
    cp5b.addButton("reset").setPosition(10,60).setSize(100,19).setCaptionLabel("Reset Device")
                                .plugTo(parent).moveTo("Advanced");
                                
    cp5b.addTextfield("patternpos").setPosition(10, 100).setSize(50,19).setValue("b").setCaptionLabel("Vibration Sequence b-d")
                                 .plugTo(parent).moveTo("Advanced");
                                 
    cp5b.addTextfield("patternpat").setPosition(10, 150).setSize(50,19).setValue("0").setCaptionLabel("Vibration Pattern 0-123")
                                   .plugTo(parent).moveTo("Advanced");
                         
    cp5b.addButton("changevibr").setPosition(10,200).setSize(130,19).setCaptionLabel("Update Vibration Pattern")
                                .plugTo(parent).moveTo("Advanced");
                                
    dropdown = cp5b.addDropdownList("Serial Ports",10,30,150,150)
                  .plugTo(parent,"dropdown").moveTo("Basic");
    loadDropdown();
    dropdown.setCaptionLabel(port);
    
    CheckBox vt = cp5b.addCheckBox("vibtype").setPosition(10,230)
                                    .addItem("Enable LRM",0)
                                    .plugTo(parent).moveTo("Advanced");
    if(vib_type.equals("lrm")) {
      vt.activate(0);
    } else {
      vt.deactivate(0);
    }
    
  }
  
  public void draw() {
    background(0);
  }
  
  
  void exit() {
    this.dispose();
    cf = null;
    minderhttp.updateSensorSettings(oauth_token, sensitivity, difficulty1, difficulty2, seatback, System.currentTimeMillis());
    //this.surface.setVisibile(false);
  }

}


private class ScheduleThread extends Thread {
  private boolean done=false;
  
  public void run() {
    while(!done) {
      try { 
        Thread.sleep(10000);
        File schedfile = dataFile("schedule.json");
        if(schedfile.exists()) {
          JSONArray jsched = loadJSONArray(dataPath("schedule.json"));
          for(int i = 0; i < jsched.size(); i++) {
            //JSONObject obj = loadJSONObject(dataPath("schedule.json"));
            JSONObject obj = jsched.getJSONObject(i);
            elog.log(i + "Found exercise: name=" + obj.getString("name") + ", time=" + obj.getString("time") +
                    ", video=" + obj.getString("video") + ", path=" + obj.getString("path"));
            long etime = Long.parseLong(obj.getString("time"));
            long curtime = System.currentTimeMillis();
            
            play_pid = obj.getString("pid");
            play_sid = obj.getString("sid");
            play_sdid = obj.getString("sdid");
            play_mid = obj.getString("mid");
            play_stime = etime;
            
            
            if(etime - curtime < 60000 && etime - curtime > 0) {
              elog.log("We should do this schedule!");
              exerciser = new ExerciseThread(obj.getString("name"),etime,obj.getString("video"),obj.getString("path"));
              exerciser.start();
            }  /*else if(i == 0) {
              exerciser = new ExerciseThread(obj.getString("name"),curtime+5000,obj.getString("video"),obj.getString("path"));
              exerciser.start();
            } */
              
          }
        }
        
        Thread.sleep(50000);
      
      } catch (InterruptedException e) {
        println(e);
        elog.log(Arrays.toString(e.getStackTrace()));
      }
    }
  }
    
  public void stopSchedule() {
    done=true;
    this.interrupt();
  }
}

private class ExerciseThread extends Thread {
  private String name;
  private long delay;
  private String videoFile;
  private String pathFile;
  
  public ExerciseThread(String n, long d, String v, String p) {
    name = n;
    exerciseName = n;
    delay = d - System.currentTimeMillis();
    videoFile = v;
    pathFile = p;
  }
  
  public void run() {
    try {
      elog.log("Starting exercise in " + delay + "ms");
      Thread.sleep(delay);
      
      if(gf == null) {
        pt = sframe.getLocationOnScreen();
        gf = new GuidanceFrame(app,300,300,"Guidance",false);
      } else { 
        gf.lb.setOpen(false);
      }
      
      elog.log("Setting the listbox to be not open");
      for(int i = 10; i >= 0; i--) {
        countdown = i;
        Thread.sleep(1000);
        
      }
      
      elog.log("start playing");
      ((Button)gf.cp5c.getController("playtoggle")).setCaptionLabel("Stop Playing");
      if(!pathFile.equals("")) {
        playlines = loadStrings(dataPath("paths/" + pathFile));
      } else {
        playlines = new String[0];
      }
      playindex = 0;
      playscore1 = 0;
      playscore2 = 0;
      pdisplay_x = new ArrayList<Float>();
      pdisplay_y = new ArrayList<Float>();
    
      elog.log("opening movie " + dataPath("video/"+videoFile));
      myMovie = new Movie(app, dataPath("video/"+videoFile));
      elog.log("movie object created");
      myMovie.play();
      elog.log("starting to play movie");
      playUntil = (long)myMovie.duration() * 1000 + System.currentTimeMillis();
      elog.log("playuntil = " + playUntil + ", current time = " + System.currentTimeMillis());
      
      isPlaying = true;
      elog.log("isPlaying set true");
      comparex = display_x;
      comparey = display_y;
      
    } catch (InterruptedException e) {
      println(e);
      elog.log(Arrays.toString(e.getStackTrace()));
    }
  }
  
}
        
        
        

public void chooseFile() {
  elog.log("Hey, this button was pressed chooseFile");
}

void startSerial() {
  elog.log("Starting serial port connection");
  // open serial port
  try {
    watcher = new WatcherThread(1, wListener);
    watcher.start();
    myPortAcc = new Serial(this, port, 921600);
    bytecount = 0;
    lastbytecount = System.currentTimeMillis();
    myPortAcc.buffer(20);
    myPortAcc.write("w");
    Thread.sleep(100);
    myPortAcc.write("V");
    currentState = State.DATACOLLECT;
    //currentState = State.STARTCALIBRATION; 
    elog.log("Trying to start angle data");
    retries = 0;
  }
  catch (Exception e) {
    e.printStackTrace();
    currentState = State.ERROR;
  }
}



void startCalibration() {
  if(yellowAlert != null) {
    yellowAlert.stopAlert();
  }
  
  // initialize all calibration variables
  flag_calibrate = true;
  calibrate_timer=millis();
  cali_cnt=0;
  
  x=0;
  y=0;
  z=0;
  x_former=0;
  y_former=0;
  z_former=0;
  r=0;
  dx=0;
  dy=0;
  dz=0;
  y0=0;
  z0=0;
  dyy_former=0;
  dzz_former=0;
  display_x=0;
  display_y=0;
  error_z=0;
  error_y=0;
  
  // perform calibration 
  currentState = State.CALIBRATION;
}

void performCalibration() {
  // if calibration has been running for more than 1 second and less than 7 second
  if ((millis()-calibrate_timer<=1000)) {
    // wait 1 second before starting calibration
  }
  else if ((millis()-calibrate_timer>1000) && (millis()-calibrate_timer<7000)) {
    y0 += y;
    z0 += z;
     
    cali_y[cali_cnt]=y;
    cali_z[cali_cnt]=z;
    cali_cnt += 1;
  }
  else {
    // finish calibration after 7 seconds
    currentState = State.FINISHCALIBRATION;
  }  
}

void finishCalibration() {
  // if calibration has been running for 7 seconds and currently in calibration
  z0= z0/cali_cnt;
  y0= y0/cali_cnt;
  error_z=0-z0;
  error_y=0-y0;
  deviation=0;
  for (int cntx0=0; cntx0 < cali_cnt; cntx0 ++) {
    deviation = deviation + sqrt(sq(cali_y[cntx0]+error_y) + sq(cali_z[cntx0]+error_z));
  }
  if (deviation < 3000)  {
    // calibration succeed
    cali_done=true;
    
    profileList2[curProfile].error_y = error_y;
    profileList2[curProfile].error_z = error_z;
    writeProfiles2(profile2Name);
    // write new calibration to file
    //writeProfiles(profileName);
  }
  else {
    // calibration fail
    cali_done=false;
  }
 
  // ends calibration
  flag_calibrate = false;
  currentState = State.CALIBRATIONRESULT;
}

void calibrationResult() {
  // display calibration result for 1.5 seconds
  if (millis()-calibrate_timer>8500) {
    currentState = State.DATACOLLECT;
  } 
}

void displayCalibrationTimer() {
  // change display based on how long calibration running
  if(millis()-calibrate_timer<1000){             
    textFont(f,32);                 
    text("Calibration...",50,250);  // Setup Display Text 
  }       
  else if(millis()-calibrate_timer<2000){             
    textFont(f,32);                 
    text("Calibration...",50,250);  // Setup Display Text 
    textFont(f,150);
    text("5",95,205);       
  }  
  else if(millis()-calibrate_timer<3000){   
    textFont(f,32);                 
    text("Calibration...",50,250);  // Setup Display Text            
    textFont(f,150);
    text("4",95,205); 
  }   
  else if(millis()-calibrate_timer<4000){ 
    textFont(f,32);                 
    text("Calibration...",50,250);  // Setup Display Text            
    textFont(f,150);
    text("3",95,205); 
  } 
  else if(millis()-calibrate_timer<5000){ 
    textFont(f,32);                 
    text("Calibration...",50,250);  // Setup Display Text            
    textFont(f,150);
    text("2",95,205); 
  } 
  else if(millis()-calibrate_timer<6000){ 
    textFont(f,32);                 
    text("Calibration...",50,250);  // Setup Display Text            
    textFont(f,150);
    text("1",95,205); 
  } 
  else if(millis()-calibrate_timer<7000){  
    textFont(f,32);                 
    text("Calibration...",50,250);  // Setup Display Text            
    textFont(f,150);
    text("0",95,205); 
  }
  else  {  
    if (cali_done)  { 
      textFont(f,32);                 
      text("CALIBRATION ",50,130);  // Setup Display Text    
      text("COMPLETED!",52,175);  // Setup Display Text     
    }                        
    else {
      textFont(f,32); 
      fill(255, 0, 0);                
      text("REDO ",50,130);  // Setup Display Text    
      text("CALIBRATION!",52,175);  // Setup Display Text  
      fill(255); 
    }
  }
}

void dataCollection() {
  // output current data to file
  output.print(x);
  output.print(",");
  output.print(y);
  output.print(",");
  output.print(z);
  output.print(",");
  output.print(dx);
  output.print(",");
  output.print(dy);
  output.print(",");
  output.print(dz);
  output.print(",");
  output.print(degrees(asin(dy)));
  output.print(",");
  output.print(degrees(asin(dz)));
  output.print(",");
  output.print(int(flag_tilting)+","+int(flag_tilting2)+","+int(flag1m)+","+int(flag2s)+",");
  output.print(System.currentTimeMillis());
  output.println("");  //new line and skip a line
  linecount++;
  
  // recording
  if(isRecording) {
    routput.print(x);
    routput.print(",");
    routput.print(y);
    routput.print(",");
    routput.print(z);
    routput.print(",");
    routput.print(dx);
    routput.print(",");
    routput.print(dy);
    routput.print(",");
    routput.print(dz);
    routput.print(",");
    routput.print(degrees(asin(dy)));
    routput.print(",");
    routput.print(degrees(asin(dz)));
    routput.print(",");
    routput.print(int(flag_tilting)+","+int(flag_tilting2)+","+int(flag1m)+","+int(flag2s)+",");
    routput.print(System.currentTimeMillis());
    routput.println("");  //new line and skip a line
  }
  
  // Check if it's time to send
  if(linecount == 600) {
    elog.log("Logged 1200 lines, moving file to complete folder");
    output.close();
    File curfile = new File(dataPath(dataName));
    File dataFolder = new File(dataPath("complete"));
    File newfile = new File(dataPath("complete/data_" + System.currentTimeMillis() + ".csv"));
    //File newfile = new File(dataPath("complete/data.csv"));
    if(!dataFolder.exists()) {
      dataFolder.mkdir();
    }
    curfile.renameTo(newfile);
    elog.log("Starting Upload");
    minderhttp.uploadData(oauth_token);
    elog.log("Starting new log file");
    output = createWriter(dataPath(dataName));
    linecount = 0;
  }
  
 
  stroke(220);
  strokeWeight(1.5);
  textFont(f,32); 
  int alertState = -1;

  if (sqrt(dy*dy+dz*dz)>(100-difficulty1)*0.6){ 
    // Ball is in yellow or red or laid back
    //println("dy*200="+dy*200 +", dz*200="+dz*200);
    if(seatback == 1 && ((dz>0) &&
                         (abs(dy)<(100-difficulty1)*0.6) &&
                         ((abs(dz)<(150*seatback1/100)) ||
                         (sqrt(dy*dy+(dz-150*seatback1/100)*(dz-150*seatback1/100))<(100-difficulty1)*0.6))
                         )) {
    //if(seatback == 1 && ((dz>0)&&(sqrt(dy*dy)*200<((6+seatback2)*0.6 + (1-sqrt(dz*dz)*200/150)*((6+.6*seatback1)-(6+.6*seatback2)))))) {
      alertState = -2;
      gcount += .05;
    } else {
    
      fill(255,80,80);
  
      tint(235,255,0);  // Tint yellow
      flag_tilting=true;
      flag_tilting2=false;
      alertState = 0;
      //if (sqrt(dy*dy+dz*dz)*200>42+percentage*0.6){
      if (sqrt(dy*dy+dz*dz)>60+(100-difficulty2)*0.6){
        // Ball is in red
        tint(255,76,56);  // Tint RED
        flag_tilting2 = true;
        alertState = 1;
        rcount += .05;
      } else {
        ycount += .05;
      }
    }

  }
  else{
    gcount += .05;
    tint(0,255,8);  // Tint GREEN
    flag_tilting=false;
    flag_tilting2=false;
  }
  
  ellipseMode(RADIUS);  // Set Mode to RADIUS
  //image(img1, 42, 42,216,216);
  noTint();
  //image(img2, display_x, display_y,60,60);
  
  
  // draw guides
  /*if(alertState == -1) {
    fill(30,255,30);
  } else if(alertState == 0) {
    fill(240,255,30);
  } else if(alertState == 1) {
    fill(255,30,30);
  } else if(alertState == -2) {
    fill(30,30,255);
  }*/
  fill(0);
  //fill(45,255,0);  // Set fill to white
  //stroke(255,46,0);
  stroke(255,255,255);
  strokeWeight(3);
  
  // don't draw background if we're playing back
  
  if(!isPlaying) {
    //ellipse(150,150, 72+percentage*.6, 72+percentage*.6);  // Draw white ellipse using RADIUS mode
    ellipse(150,150, 90+(100-difficulty2)*.6, 90+(100-difficulty2)*.6);  // Draw white ellipse using RADIUS mode
    
    if(seatback == 1) {
      // Draw rectangle for seated area
      /*if(alertState == -1) fill(30,255,30,126);
      if(alertState == 0) fill(240,255,30);
      if(alertState == 1) fill(255,30,30);
      if(alertState == -2) fill(30,30,255);
      */
      fill(0);
      
      //stroke(255,255,23);
      stroke(255,255,255);
      strokeWeight(3);
      ellipse(150,150+150*(seatback1/100), 30+(100-difficulty1)*.6, 30+(100-difficulty1)*.6); 
      
      // Draw middle seatback section
      noStroke();
      rect(150-(30+(100-difficulty1)*.6),150,(30+(100-difficulty1)*.6)*2,150*(seatback1/100));
      
      //Draw back seat lines
      //stroke(255,255,23);
      stroke(255,255,255);
      strokeWeight(3);
      line(150-(30+(100-difficulty1)*.6),150,150-(30+(100-difficulty1)*.6),150+150*(seatback1/100));
      line(150+(30+(100-difficulty1)*.6),150,150+(30+(100-difficulty1)*.6),150+150*(seatback1/100));
      
    }
    // draw inner circle
    //stroke(255,255,23);
    if(alertState == -1) fill(30,255,30,126);
    if(alertState == 0) fill(240,255,30,126);
    if(alertState == 1) fill(255,30,30,126);
    if(alertState == -2) fill(30,30,255,126);
    stroke(255,255,255);
    strokeWeight(3);
    ellipse(150,150, 30+(100-difficulty1)*.6, 30+(100-difficulty1)*.6);  // Draw white ellipse using RADIUS mode

  }
  
  // draw play ball
  if(isPlaying) {
    if(playindex > playlines.length) {
      isPlaying = false;
      playindex = 0;
      ((Button)gf.cp5c.getController("playtoggle")).setCaptionLabel("Play");
      pdisplay_x.clear();
      pdisplay_y.clear();
      elog.log("Finished playback, final score is " + playscore1 + "/" + playscore2);
      int playresult = 0;
      if(playscore1 >= playscore2 * 1.1) {
        playresult = 1;
      }
      play_ctime = System.currentTimeMillis();
      minderhttp.sendActivityResult(oauth_token, ""+playresult, play_pid, play_sid, play_sdid, play_mid, play_stime, play_ctime);
      
      
    } else if (playlines.length == 0 ) {
      //elog.log("bloop");
      playscore1 += sqrt((display_x-comparex)*(display_x-comparex) + (display_y-comparey) * (display_y-comparey));
      comparex = display_x;
      comparey = display_y;
      //elog.log("display_x = " + display_x + ", comparex = " + comparex + ", playscore1 = " + playscore1);
    } else {
     String templine = playlines[playindex];
      playindex++;
      String[] parts = split(templine,',');
      //pdisplay_x.add(0,120+Float.parseFloat(parts[4])*200);
      //pdisplay_y.add(0,120+Float.parseFloat(parts[5])*200);
      pdisplay_x.add(0,150+Float.parseFloat(parts[4]));
      pdisplay_y.add(0,150+Float.parseFloat(parts[5]));
      if(pdisplay_x.size() > 10) {
        pdisplay_x.remove(10);
        pdisplay_y.remove(10);
      }
      
      // calculate score
      float nowscore = 2*(150 - 5*sqrt(abs(pdisplay_x.get(0)-display_x)*abs(pdisplay_x.get(0)-display_x) + 
                            abs(pdisplay_y.get(0)-display_y) * abs(pdisplay_y.get(0)-display_y)));
      playscore1 = ((playindex-1)*playscore1 + nowscore)/playindex;
      float cnowscore = 2*(150 - 5*sqrt(abs(pdisplay_x.get(0)-comparex)*abs(pdisplay_x.get(0)-comparex) + 
                            abs(pdisplay_y.get(0)-comparey) * abs(pdisplay_y.get(0)-comparey)));
      
      playscore2 = ((playindex-1)*playscore2 + cnowscore)/playindex;
      
      for(int i = 0; i < pdisplay_x.size(); i++) {
        stroke(128,128,128);
        strokeWeight(3);
        fill(128,128,128);
        //ellipse(pdisplay_x.get(i)+30,pdisplay_y.get(i)+30,40,40);
        ellipse(pdisplay_x.get(i),pdisplay_y.get(i),40,40);
      } 
      
    }
  }
    
  
  // Draw center ball
  stroke(255,255,255);
  strokeWeight(3);
  fill(255,255,255);
  ellipse(display_x,display_y,30,30);
  
  if(!isPlaying && !isRecording) {
    // Manage alert thread
    if(alertState >= 0) {
      if(yellowAlert == null) {
        int[] gains = {volume1, volume2};
        String[] sns = {qqName[alert1sound],qqName[alert2sound]};
        byte vibcode = 86;
        if(vib_type.equals("lrm")) {
          vibcode = 76;
        }
        yellowAlert = new AlertThread(elog,intervalTimes[profileList2[curProfile].interval1]*1000
                            , 1000*intervalTimes[profileList2[curProfile].interval2]
                            , sns
                            ,new Minim(this)
                            ,gains,alertState, myPortAcc, alarmActive, valarmActive, vib1, vib2, vibcode);
        yellowAlert.start();
      } else {
        yellowAlert.resetGood();
        yellowAlert.newState(alertState);
      }
    } else {
      if(yellowAlert != null) {
        if(yellowAlert.addGood()) {
          yellowAlert = null;
        }
      }
    }
  }

}

// handle serial data
void serialEvent(Serial thisPort) {
  bytecount += 1;
  if(bytecount >= 1000) {
    long newlastbytecount = System.currentTimeMillis();
    elog.log("Received 1KByte in " + (newlastbytecount - lastbytecount) + "ms");
    lastbytecount = newlastbytecount;
    bytecount = 0;
  }
  if (thisPort == myPortAcc) {
    if(watcher != null) {
      watcher.gotData();
    }
    int inByteacc = myPortAcc.read();
    //println("received byte" + inByteacc);
    if (former==126 && inByteacc==0){
      //println("got an accel packet!");
      flag1=true;
      cnt=10;
    }
    
    // Check on angle data from new sensor
    if(former == 126 && inByteacc == 7) {
      //println("Got a angle packet!");
      flagAngle = true;
      flagCommand = false;
      cnt = 10;
    }
    
    if(former == 126 && inByteacc == 4) {
      flagCommand = true;
      cnt = 10;
    }
    
    if(flagCommand) {
      switch(cnt) {
        case 10:
          cnt =1;
          elog.log("command reply byte " + inByteacc);
          break;
        case 1:
          elog.log("command reply byte " + inByteacc);
          cnt = 2;
          break;
        case 2:
          elog.log("command reply byte " + inByteacc);
          flagBattery = inByteacc == 112;
          cnt = 3;
          break;
        case 3:
          elog.log("command reply byte " + inByteacc);
          if(flagBattery) {
            elog.log("Battery level = " + inByteacc);
            batteryText.setText(inByteacc+"%");
          }
          cnt = 4;
          break;
        default:
          flagCommand = false;
          cnt = 0;
          break;
      }
    }
    
    if(flagAngle) {
      switch(cnt) {
        case 10:
          cnt = 1;
          break;
        case 1:
          x = inByteacc;
          cnt += 1;
          if(x > 127) {
            x = x - 256;
          }
          break;
        case 2:
          y = inByteacc + error_y;
          cnt += 1;
          if(y > 127) {
            y = y - 256;
          }
          break;
        case 3:
          z = inByteacc + error_z;
          cnt += 1;
          if(z > 127) {
            z = z - 256;
          }
          break;
        default:
          flagAngle = false;
          cnt = 0;
          
          if (abs(x)>3000 || abs(y)>3000 || abs(z)>3000){
            x=x_former;
            y=y_former;
            z=z_former;
          }
          else{
            x_former=x;
            y_former=y;
            z_former=z;
          }
          
          dy = y * 0.02 * sensitivity*1.5;
          dz = z * 0.02 * sensitivity*1.5;

        
          // Smoothing
          dy=0.8*dyy_former+0.2*dy;
          dz=0.8*dzz_former+0.2*dz;
          dyy_former=dy;
          dzz_former=dz;

          display_x=150+dy;
          display_y=150+dz;
          
          //println("new sensor");
          
          break;
          
      }
    }
    
    
    if (flag1==true){
      switch (cnt){
        case 10:
          cnt=1;
          break;
        case 1:
          x1=inByteacc;
          cnt += 1;  
          break;
        case 2:
          x2=inByteacc;
          cnt += 1; 
          break;
        case 3:
          y1=inByteacc;
          cnt += 1;  
          break;
        case 4:
          y2=inByteacc;
          cnt += 1;         
          break;
        case 5:
          z1=inByteacc;
          cnt += 1;         
          break;
        case 6:
          z2=inByteacc;
          cnt += 1;         
          break;
        default:
          flag1=false;
          cnt = 0;
          
          if (flag_calibrate){
            x=x1*256+x2;
            if (x>32767){
              x=x-65536;
            }
          
            y=y1*256+y2;
            if (y>32767){
              y=y-65536;
            }
  
            z=z1*256+z2;
            if (z>32767){
              z=z-65536;
            }
            if (abs(x)>3000 || abs(y)>3000 || abs(z)>3000){
              x=x_former;
              y=y_former;
              z=z_former;
            }
            else{
              x_former=x;
              y_former=y;
              z_former=z;
            }
          }
          else{
            x=x1*256+x2;
            if (x>32767){
              x=x-65536;
            }
        
            y=y1*256+y2+error_y;
            if (y>32767){
              y=y-65536;
            }
  
            z=z1*256+z2+error_z;
            if (z>32767){
              z=z-65536;
            }
            if (abs(x)>3000 || abs(y)>3000 || abs(z)>3000){
              x=x_former;
              y=y_former;
              z=z_former;
            }
            else{
              x_former=x;
              y_former=y;
              z_former=z;
            }
          }
  
          r=sqrt(x*x+y*y+z*z);
          dx=x/r;
          dy=y/r*0.02*sensitivity;
          dz=z/r*0.02*sensitivity;
        
          dy=0.95*dyy_former+0.05*dy;
          dz=0.95*dzz_former+0.05*dz;
          dyy_former=dy;
          dzz_former=dz;
        
          //if (abs(dyy_former-dy)<0.005||abs(dzz_former-dz)<0.005){
          display_x=120+dyy_former*200;//120+(0.95*dyy_former+0.05*dy)*200;
          display_y=120+dzz_former*200;//120+(0.95*dzz_former+0.05*dz)*200;
          
          break;
      }
    }
  former=inByteacc;
  } else {
    elog.log("wrong port?");
  }
}

void controlEvent(ControlEvent theEvent) {
  if(theEvent.isAssignableFrom(Textfield.class)) {
    elog.log("text field changed!");
  }
  if(theEvent.isGroup()) {
    g2IsOpen=theEvent.getGroup().isOpen();
    
    elog.log("Event: " + theEvent.getGroup().getValue() + " from: " + theEvent.getGroup());
    

  } else {
    String eventName = theEvent.getController().getName();
    //String eventValue = theEvent.getController().getValue();
    
    elog.log("event from controller : "+theEvent.getController().getValue()+" from "+eventName);
    if(eventName.equals("Serial Ports")) {
      int portnum = (int)theEvent.getController().getValue();
      if(portnum < whiports.size()) {
        port = whiports.get((int)theEvent.getController().getValue());
        elog.log("New port = " + port);
        // open serial port
        try {
          if(myPortAcc != null) {
            myPortAcc.clear();
            myPortAcc.stop();
          }
          if(watcher != null) {
            watcher.stopWatcher();
          }
          
          watcher = new WatcherThread(1, wListener);
          watcher.start();
          myPortAcc = new Serial(this, port, 921600);
          myPortAcc.buffer(20);  
          currentState = State.DATACOLLECT;
          retries = 0;
        }
        catch (Exception e) {
          e.printStackTrace();
          currentState = State.ERROR;
          //watcher = new WatcherThread(0, wListener);
          //watcher.start();
          //g2.setOpen(false);
        }
        writeConfig(configName);
      } else if (portnum == whiports.size()) {
        elog.log("Refreshsing serial ports");
        getWhiPorts();
        loadDropdown();
      }
    } else if(eventName.equals("Settings")) {
      if(cf == null) {
        pt = sframe.getLocationOnScreen();
        cf = new ControlFrame(this,400,300,"App Settings");
      }
    } else if(eventName.equals("Guidance")) {
      if(gf == null) {
        pt = sframe.getLocationOnScreen();
        gf = new GuidanceFrame(this,300,300,"Guidance", true);
      } else {
        elog.log("Whoops, there's already a guidance frame, no need to open a new one");
      }
    } else if(eventName.equals("Data")) {
      
      // Init data
      for(int i=0; i<24; i++) {
        for(int j=0; j<3; j++) {
          sumdata[i][j] = 0;
        }
      }
      
        
       // Draw graph
       // Read data in dailydata and take the last 24 values
      String[] summarylines = loadStrings(dataPath("dailydata.csv"));
      int count = 0;
      int startcount = summarylines.length - 24;
      if(startcount < 0) {
        startcount = 0;
      }
      for(int i=startcount; i<summarylines.length; i++) {
        String[] parts = split(summarylines[i],",");
        sumdata[count][0] += Integer.parseInt(parts[1]);
        sumdata[count][1] += Integer.parseInt(parts[2]);
        sumdata[count][2] += Integer.parseInt(parts[3]);
        //if((i+1)%5 == 0) {
        count++;
        //}
      }
          
      if(df == null) {
        pt = sframe.getLocationOnScreen();
        df = new DataFrame(this,600,110,"Data");
      }
    } else if(eventName.equals("Audio")) {
      alarmActive = !alarmActive;
      if(alarmActive) {
        audioButton.setCaptionLabel("Sound");
        if(yellowAlert != null) {
          yellowAlert.active = true;
        } 
      } else {
        audioButton.setCaptionLabel("No Sound");
        if(yellowAlert != null) {
          yellowAlert.active = false;
        }
      }
    } else if(eventName.equals("Close")) {
      exit();
      //System.exit(0);
    } else if(eventName.equals("Hide")) {
      sframe.setExtendedState(Frame.ICONIFIED);
    } else if(eventName.equals("Vibration")) {
      valarmActive = !valarmActive;
      if(valarmActive) {
        vibrationButton.setCaptionLabel("Vibration");
        if(yellowAlert != null) {
          yellowAlert.vactive = true;
        }
      } else {
        vibrationButton.setCaptionLabel("No Vibration");
        if(yellowAlert != null) {
          yellowAlert.vactive = false;
        }
      }
    }else if(eventName.equals("recordtoggle")) {
      elog.log("toggling record");
      if(isRecording) {
        elog.log("stop recording");
        ((Button)gf.cp5c.getController("recordtoggle")).setCaptionLabel("Record");
        isRecording = false;
        routput.close();
        File oldFile = dataFile("recording.csv");
        lastrecname = "paths/recording_" +System.currentTimeMillis()+".csv";
        File newFile = dataFile(lastrecname);
        oldFile.renameTo(newFile);
        //cam.stop();
        if(app == null) {
          elog.log("app is null, can't do selectoutput");
        } else {
          elog.log("app isn't null, we're doing selectoutout");
          String defaultfilename = System.getProperty("user.home") + "/recording.csv";
          File defaultfile = new File(defaultfilename);
          app.selectOutput("Save recording to: ", "recordFileSelected", defaultfile);
          //app.selectOutput("Save recording to: ", "recordFileSelected", newFile);
        }
      } else {
        elog.log("start recording");
        ((Button)gf.cp5c.getController("recordtoggle")).setCaptionLabel("Stop Recording");
        routput = createWriter(dataPath("recording.csv"));
        File videofolder = new File(dataPath("video"));
        if(!videofolder.exists()) {
          videofolder.mkdir();
        }
        
        isRecording = true;
        //videoFrameCount = 0;
        //cam = new Capture(this, 320, 180, 30);
        //cam.start();

      }
    } else if(eventName.equals("playtoggle")) {
      elog.log("toggling playing");
      if(isPlaying) {
        elog.log("stop playing");
        ((Button)gf.cp5c.getController("playtoggle")).setCaptionLabel("Play");
        isPlaying = false;
      } else {
        elog.log("start playing");
        ((Button)gf.cp5c.getController("playtoggle")).setCaptionLabel("Stop Playing");
        playlines = loadStrings(dataPath("recording.csv"));
        isPlaying = true;
        playindex = 0;
        playscore1 = 0;
        playscore2 = 0;
        pdisplay_x = new ArrayList<Float>();
        pdisplay_y = new ArrayList<Float>();
        myMovie = new Movie(this, dataPath("video/video.mov"));
        myMovie.play();
        playUntil = (long)myMovie.duration() * 1000 + System.currentTimeMillis();
      }
    } else if(eventName.equals("updatename")) {
      String newname = ((Textfield)cf.cp5b.getController("name")).getText();
      elog.log("Changing activity name to " + newname);
      name = newname;
      for(int i = 0; i < 5; i++) {
        profileRadio.removeItem(profileList2[i].name);
      }
      profileList2[curProfile].name = name;
      for(int i = 0; i < 5; i++) {
        profileRadio.addItem(profileList2[i].name,i);
      }
      profileRadio.activate(curProfile);
    } else if(eventName.equals("login")) {
      String tempname = ((Textfield)cf.cp5b.getController("username")).getText();
      String temppassword = ((Textfield)cf.cp5b.getController("password")).getText();
      ((Textlabel)cf.cp5b.getController("loginmessage")).setText("Logging in...");
      //LoginThread upper = new LoginThread(tempname, temppassword, loginCallback);
      //upper.start();
      minderhttp.loginUser(tempname, temppassword);
    } else if(eventName.equals("logout")) {
      username = "";
      oauth_token = "";
      oauth_expiry = "";
      writeConfig(configName);
      ((Textfield)cf.cp5b.getController("username")).setValue(username);
      ((Textlabel)cf.cp5b.getController("loginmessage")).setText("Not logged in");
    } else if(eventName.equals("programs")) {
      //Programs p = new Programs(username, oauth_token);
      //p.getPrograms();
      minderhttp.updatePrograms(oauth_token);
    } else if(eventName.equals("register")) {
      //RegisterThread regger = new RegisterThread(oauth_token, deviceid);
      elog.log("Attempting to register device");
      //regger.start();
      minderhttp.registerDevice(oauth_token, deviceid);
    } else if(eventName.equals("reset")) {
      elog.log("Attempting to reset sensor");
      if(myPortAcc != null) {
        try {
          myPortAcc.write(119);
          Thread.sleep(100);
          myPortAcc.write(120);
          elog.log("Sensor reset sent...");
        } catch (InterruptedException e) {
          println("Interrupted during write sleep timer");
          elog.log(Arrays.toString(e.getStackTrace()));
        }
      }
    } else if(eventName.equals("changevibr")) {
      String newpatternpos = ((Textfield)cf.cp5b.getController("patternpos")).getText();
      String newpattern = ((Textfield)cf.cp5b.getController("patternpat")).getText();
      byte newpatternbyte = (byte)Integer.parseInt(newpattern);
      elog.log("Updating Pattern " + newpatternpos + " to " + newpatternbyte);    
      if(myPortAcc != null) {
        try {
          myPortAcc.write("w");
          Thread.sleep(100);
          myPortAcc.write(newpatternpos);
          Thread.sleep(100);
          myPortAcc.write(newpatternbyte);
        } catch( InterruptedException e) {
          println("Interrupted during write sleep timer");
          elog.log(Arrays.toString(e.getStackTrace()));
        }
      }
    } 
  }
  
  if (currentState != State.SETUP) {
    // When values in the control panel are changed, these values are updated;
    
    elog.log("Updating values!" + " name = " + name);
    
    //profileList2[curProfile].name = name;
    profileList2[curProfile].difficulty1 = difficulty1;
    if(redyellow == 1 && difficulty1*2 != difficulty2) {
      difficulty2 = difficulty1 * 2;
      cf.cp5b.getController("difficulty2").setValue(difficulty2);
    }
    profileList2[curProfile].difficulty2 = difficulty2;
    profileList2[curProfile].alert1sound = alert1sound;
    profileList2[curProfile].alert2sound = alert2sound;
    profileList2[curProfile].volume1 = volume1;
    profileList2[curProfile].volume2 = volume2;
    profileList2[curProfile].interval1 = interval1; //intervalTimes[interval1];
    profileList2[curProfile].interval2 = interval2;
    profileList2[curProfile].vpattern1 = vpattern1;
    profileList2[curProfile].vpattern2 = vpattern2;
    profileList2[curProfile].vstrength1 = vstrength1;
    profileList2[curProfile].vstrength2 = vstrength2;
    profileList2[curProfile].redyellow = redyellow;
    profileList2[curProfile].seatback = seatback;
    profileList2[curProfile].sensitivity = sensitivity;
    profileList2[curProfile].seatback1 = seatback1;
    profileList2[curProfile].seatback2 = seatback2;
    profileList2[curProfile].vib1 = vib1;
    profileList2[curProfile].vib2 = vib2;
    
    
    // write new calibration to file
    //writeProfiles(profileName);
    writeProfiles2(profile2Name);
    writeConfig(configName);
  }
  
}

void recordFileSelected(File selection) {
  if(selection == null) {
    elog.log("no save file selected");
  } else {
    elog.log("saving " + lastrecname + " to new file " + selection.getAbsolutePath());
    File oldfile = dataFile(lastrecname);
    oldfile.renameTo(selection);
  }
}

void vib1enable(float a[]) {
  vib1 = int(a[0]);
  profileList2[curProfile].vib1 = vib1;
  writeProfiles2(profile2Name);
}

void vib2enable(float a[]) {
  vib2 = int(a[0]);
  profileList2[curProfile].vib2 = vib2;
  writeProfiles2(profile2Name);
}

void redtoyellow(float a[]) {
  elog.log(""+a);
  redyellow = int(a[0]);
  profileList2[curProfile].redyellow = redyellow;
  if(redyellow == 1) {
    difficulty2 = difficulty1 * 2;
    cf.cp5b.getController("difficulty2").setValue(difficulty2);
  }
  profileList2[curProfile].difficulty2 = difficulty2;
  writeProfiles2(profile2Name);
}

// Called every time a new frame is available to read
void movieEvent(Movie m) {
  m.read();
}

void vibtype(float a[]) {
  elog.log("new vibtype = " + a[0]);
  if(a[0] == 0) {
    vib_type = "erm";
  } else {
    vib_type = "lrm";
  }
  writeConfig(configName);
}

void seatback(float a[]) {
  seatback = int(a[0]);
  profileList2[curProfile].seatback = seatback;
  writeProfiles2(profile2Name);
}
void radio(int a) {
  elog.log("radio button: "+a);
  curProfile = a;
  name = profileList2[curProfile].name;
  difficulty1 = profileList2[curProfile].difficulty1;
  difficulty2 = profileList2[curProfile].difficulty2;
  alert1sound = profileList2[curProfile].alert1sound;
  alert2sound = profileList2[curProfile].alert2sound;
  volume1 = profileList2[curProfile].volume1;
  volume2 = profileList2[curProfile].volume2;
  interval1 = profileList2[curProfile].interval1;
  interval2 = profileList2[curProfile].interval2;
  vpattern1 = profileList2[curProfile].vpattern1;
  vpattern2 = profileList2[curProfile].vpattern2;
  vstrength1 = profileList2[curProfile].vstrength1;
  vstrength2 = profileList2[curProfile].vstrength2;
  redyellow = profileList2[curProfile].redyellow;
  seatback= profileList2[curProfile].seatback;
  sensitivity = profileList2[curProfile].sensitivity;
  seatback1 = profileList2[curProfile].seatback1;
  seatback2 = profileList2[curProfile].seatback2;
  vib1 = profileList[curProfile].vib1;
  vib2 = profileList[curProfile].vib2;
  
  ((Textfield)cf.cp5b.getController("name")).setValue(name);
  cf.cp5b.getController("difficulty1").setValue(difficulty1);
  cf.cp5b.getController("difficulty2").setValue(difficulty2);
  cf.cp5b.getController("volume1").setValue(volume1);
  cf.cp5b.getController("volume2").setValue(volume2);
  cf.cp5b.getController("alert1sound").setValue(alert1sound);
  cf.cp5b.getController("alert2sound").setValue(alert2sound);
  cf.cp5b.getController("interval1").setValue(interval1);
  cf.cp5b.getController("interval1").setCaptionLabel(intervalNames[interval1]);
  cf.cp5b.getController("interval2").setValue(interval2);
  cf.cp5b.getController("interval2").setCaptionLabel(intervalNames[interval2]);
  cf.cp5b.getController("vpattern1").setValue(vpattern1);
  cf.cp5b.getController("vpattern2").setValue(vpattern2);
  cf.cp5b.getController("vstrength1").setValue(vstrength1);
  cf.cp5b.getController("vstrength2").setValue(vstrength2);
  cf.cp5b.getController("seatback1").setValue(seatback1);
  cf.cp5b.getController("seatback2").setValue(seatback2);
  if(redyellow == 1) {
    cf.cb.activate(0);
  }  else {
    cf.cb.deactivate(0);
  }
  if(seatback == 1) {
    cf.sbcb.activate(0);
  } else {
    cf.sbcb.deactivate(0);
  }
  cf.cp5b.getController("sensitivity").setValue(sensitivity);
  
  if(vib1 == 1) {
    cf.vb1.activate(0);
  } else {
    cf.vb1.deactivate(0);
  }
  
  if(vib2 == 1) {
    cf.vb2.activate(0);
  } else {
    cf.vb2.deactivate(0);
  }
  
  error_y = profileList2[curProfile].error_y;
  error_z = profileList2[curProfile].error_z;
  
}

void keyPressed() {
  if (keyCode == ALT && isAltPressed == false) isAltPressed = true;
  if (char(keyCode)=='C') {
    //if (isAltPressed && currentState != State.CALIBRATION) {
    if (isAltPressed) {
      currentState = State.STARTCALIBRATION; 
    }
  }
  
}

void keyReleased() {
  if (keyCode == ALT) isAltPressed = false; 
}

void loadDropdown() {
  dropdown.clear();
  for(int i = 0; i < whiports.size(); i++) {
    dropdown.addItem(whiports.get(i),i);
  }
  dropdown.addItem("Refresh Ports",whiports.size());
  dropdown.close();
  //dropdown.setGroup(g2);
  
}

// function to load configuration file
// line 1: serial port
// line 2: index of last profile used
// line 3: show/hide setting menu
void loadConfig(String filename) {
  

  getWhiPorts();
  elog.log(dataPath(filename));
  File file = new File(dataPath(filename));
  String[] lines;
  if (!file.exists()) {
    elog.log("File not found.");
    return;
  }
  else {
    lines = loadStrings(dataPath(filename));
    port = lines[0];
    elog.log("Port = " + port);
    curProfile = int(lines[1]);
    g2IsOpen = boolean(lines[2]);
    vib_type = lines[3];
    if(lines.length < 7) {
      username ="";
      oauth_token="";
      oauth_expiry="";
    } else {
      username = lines[4];
      oauth_token = lines[5];
      oauth_expiry = lines[6];
    }
  }
}

void getWhiPorts() {
    // List serial ports
  String[] ports = Serial.list();
  whiports = new ArrayList<String>();
  
  for(int i = 0; i < ports.length; i++) {
    boolean iswhi = ports[i].contains("WHI");
    boolean istty = ports[i].contains("tty");
    boolean iscom = ports[i].contains("COM");
    if((iswhi && istty) || iscom) {
      elog.log("Found port: " + ports[i]);
      whiports.add(ports[i]);
    }
    
  }
}

// New Profile Function
void loadProfiles2(String filename) {
  elog.log(dataPath(filename));
  File file = new File(dataPath(filename));
  String[] lines;
  if (!file.exists()) {
    for (int i=0; i<5; i++) {
      profileList2[i] = new Profile("P-"+i); 
    }
  }
  else {
    lines = loadStrings(dataPath(filename));
    elog.log(""+lines.length);
    for (int i=0; i<lines.length; i++) {
      String[] part = split(lines[i],',');
      profileList2[i] = new Profile(part[0],float(part[1]), float(part[2]), float(part[3]), float(part[4])
                                    , int(part[5]), int(part[6]), int(part[7]), int(part[8]), int(part[9])
                                    , int(part[10]), int(part[11]), int(part[12]), int(part[13]), int(part[14])
                                    , int(part[15]), int(part[16]), float(part[17]), float(part[18]),float(part[19])
                                    , int(part[20]), int(part[21]));
    }
    for (int i=lines.length; i<5; i++) {
      profileList2[i] = new Profile("P-"+i); 
    }
  }
  for (int i=0; i<5; i++) {
    elog.log(i+": "+profileList2[i].toString());   
  }
}

void writeConfig(String filename) {
  PrintWriter output = createWriter(dataPath(filename));
  output.println(port);
  output.println(curProfile);
  output.println(g2IsOpen);
  output.println(vib_type);
  output.println(username);
  output.println(oauth_token);
  output.println(oauth_expiry);
  output.flush();
  output.close();
}

/*
void writeProfiles(String filename) {
  PrintWriter output = createWriter(dataPath(filename));
  for (int i=0; i<5; i++) {
    output.println(profileList[i].toString());   
  }
  output.flush();
  output.close();
}*/

void writeProfiles2(String filename) {
  PrintWriter output = createWriter(dataPath(filename));
  for (int i=0; i<5; i++) {
    output.println(profileList2[i].toString2());   
  }
  output.flush();
  output.close();
}

void exit() {
  elog.log("exiting....");
  
  if(yellowAlert != null) {
    yellowAlert.stopAlert();
  }
  
  if(watcher != null) {
    watcher.stopWatcher();
  }
  
  if(scheduler != null) {
    scheduler.stopSchedule();
  }
  
  if (currentState != State.ERROR) {

    if(isRecording) {
      routput.close();
    }
    myPortAcc.clear();
    myPortAcc.stop();
   
    //writeProfiles(profileName);
    writeProfiles2(profile2Name);
    writeConfig(configName);
  }
  
  super.exit();
  //this.dispose();
}

public void dispose() {
  elog.log("in dispose");
}

public interface MinderInterface {
  public void loginResponse(boolean success, String username, String token, String expiry);
}

class MinderCallback implements MinderInterface {
  public void loginResponse(boolean success, String uname, String token, String expiry) {
    elog.log("Login was " + success + ", username = "  + uname + ", token = " + token + ", expiry = " + expiry);
    if(success) {
      ((Textlabel)cf.cp5b.getController("loginmessage")).setText("Logged in");
      username = uname;
      oauth_token = token;
      oauth_expiry = expiry;
      writeConfig(configName);
    } else {
      ((Textlabel)cf.cp5b.getController("loginmessage")).setText("Not logged in");
    }
  }
}