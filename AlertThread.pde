import ddf.minim.*;

class AlertThread extends Thread{
  private int interval1;
  private int interval2;
  private String[] soundName;
  private boolean done = false;
  private int goodCounter;
  private int[] gain;
  private int currentState;
  
  private Minim minim;        
  private AudioPlayer soundPlayer;
  private Serial myPortAcc;
  public boolean active;
  public boolean vactive;
  boolean vib1;
  boolean vib2;
  byte vibcode;
  ErrorLogger elog;
  
  public AlertThread(ErrorLogger logger, int i1, int i2, String[] sn, Minim m, int[] g, int cs, Serial myport, boolean a, boolean v, int v1, int v2, byte vc) {
    elog = logger;
    interval1 = i1;
    interval2 = i2;
    soundName = sn;
    goodCounter = 0;
    minim = m;
    myPortAcc = myport;
    gain = g;
    currentState = cs;
    active = a;
    vactive = v;
    vib1 = v1 == 1;
    vib2 = v2 == 1;
    vibcode = vc;
  }
  
  public void run() {
    
    soundPlayer = minim.loadFile(dataPath(soundName[currentState]),2048);
    soundPlayer.setGain((int)(-13-(67-gain[currentState]/100.0*67)));
    println("Alert Thread Running with sound " + soundName[currentState] + " at gain " + gain[currentState]);
    try {
      Thread.sleep(interval1);
      println("Finished first time");
      if(goodCounter == 0) {
        if(active) {
          println("playing sound "+ soundName[currentState]);
          soundPlayer.play();
        }
        if(vactive && vib1) {
          myPortAcc.write(119);
          Thread.sleep(50);
          myPortAcc.write(vibcode);
        }
      }
    } catch(InterruptedException e) {
      // Interrupted
      done = true;
      println("Interrupted during first timer");
      elog.log(Arrays.toString(e.getStackTrace()));
    }
    while(!done) {
      try {
        Thread.sleep(interval2);
        println("Finished second timer");
        if(goodCounter == 0) {
          if(active) {
            println("playing sound " + soundName[currentState]);
            soundPlayer.rewind();
            soundPlayer.play();
          }
          if(vactive && vib2) {
            myPortAcc.write(119);
            Thread.sleep(100);
            myPortAcc.write(vibcode);
          }
        }
      } catch(InterruptedException e) {
        // Interrupted
        done = true;
        println("Interrupted during second timer");
        elog.log(Arrays.toString(e.getStackTrace()));
      }
    }
    println("Alert Thread Finished");
    soundPlayer.close();
  }
  
  public boolean addGood() {
    goodCounter += 1;
    if(goodCounter >= 40) {
      println("Got 80 good values, we can stop now");
      done = true;
      this.interrupt();
    }
    return done;
  }
  
  public void resetGood() {
    goodCounter = 0;
  }
  
  public void newState(int ns) {
    if(ns != currentState) {
      soundPlayer.close();
      soundPlayer = minim.loadFile(dataPath(soundName[ns]),2048);
      soundPlayer.setGain((int)(-13-(67-gain[ns]/100.0*67)));
    }
    currentState = ns;
  }
  
  public void setGain(int i, int newgain) {
    gain[i] = newgain;
  }
  
  public void stopAlert() {
    done = true;
    this.interrupt();
  }
}