import java.text.SimpleDateFormat;

class ErrorLogger {
  
  private PrintWriter zoutput;
  private SimpleDateFormat df;
  
  public ErrorLogger(String filename) {
  
    File f = dataFile("logs/" + filename);
    try {
      zoutput = new PrintWriter(new BufferedWriter(new FileWriter(f,true)));
    } catch (IOException e) {
      println(e);
    }
    
    df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
  }
  
  public void log(String msg) {
    println(msg);
    zoutput.write("[" + df.format(new Date()) + "]:" + msg + "\r\n");
    zoutput.flush();
  }
  
  public void close() {
    zoutput.close();
  }
  
}